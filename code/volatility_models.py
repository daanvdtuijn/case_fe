import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import math

from tabulate import tabulate
from scipy.optimize import minimize
from scipy.stats import norm, t, gennorm, kstest
from skewstudent import SkewStudent

from scipy.special import gammaln, gamma
from grad import hessian_2sided

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)

class Metrics:

    ''' Class containing simple model visualisation and performance metrics '''

    def _calculate_fmse(self, true_vola):
        # requires one input paramater: true vola
        # -> profs suggest to use (corrected) realized kernel values
        self.fse = (self.fct_vola-true_vola)**2
        self.fmse = np.sum(self.fse) / len(self.x_test)
        print(f'FMSE: {self.fmse:.2f}')

    def _calculate_fmae(self, true_vola):
        self.fae = np.abs(self.fct_vola-true_vola)
        self.fmae = np.sum(self.fae) / len(self.x_test)
        print(f'FMAE: {self.fmae:.2f}')

    def calculate_forecast_metrics(self, true_variance, scale = True):
        if scale == True:
           true_vola = np.sqrt(true_variance*self.scaler**2)
        print('\n')
        self._calculate_fmae(true_vola)
        self._calculate_fmse(true_vola)
        print('\n')
        self._plot_vola_fct(true_vola)
        print('\n')
        self._backtest_VaR()

    def _backtest_VaR(self):
        # could still add test statistic
        self.VaR_sig_levels = [.1, .05, .01]

        cov_all = []
        for a in self.VaR_sig_levels:
            VaR = self.calculate_VaR(a)
            cov = np.sum(VaR>self.x_test) / len(self.x_test)
            print(f'Coverage at {a*100}% (fraction of hits): {np.round(cov*100, 2)}%')

            cov_all.append(cov)
        self.coverage_VaR = cov_all

    def calculate_VaR(self, significance):
        if self.dist == 'Gaussian':
            return self.fct_vola * norm.ppf(significance)
        if self.dist == 'Student':
            idx = self.theta_names.index('vega')
            return self.fct_vola * t.ppf(significance, self.theta_hat[idx])
        if self.dist == 'GED':
            idx = self.theta_names.index('vega')
            return self.fct_vola * gennorm.ppf(significance, self.theta_hat[idx])
        # if self.dist == 'SST':
        #     idx1 = self.theta_names.index('vega')
        #     idx2 = self.theta_names.index('vartheta')
        #     skew = SkewStudent(eta=self.theta_hat(idx1), lam=self.theta_hat(idx2))
        #     return self.fct_vola * skew.ppf(significance)

    def _plot_vola_fct(self, true_vola):
        fig, ax = plt.subplots()
        ax.plot(true_vola[self.split_date:], label='kernel')
        ax.plot(self.fct_vola, label='predicted')
        ax.set_ylabel('variance')
        ax.set_title(f'{self.__str__}')
        ax.legend()
        plt.show()

    def plot_vola(self, display_returns=True):
        fig, ax = plt.subplots()
        if display_returns==True:
            ax.plot(self.stock_returns, label='returns', alpha=.4)
        ax.plot(self.vola, label='fitted')
        ax.plot(self.fct_vola, label='predicted')
        ax.set_title(f'{self.__str__}')
        ax.set_ylabel('volatility')
        ax.legend()
        plt.show()

class Diagnostics:

    def _calculate_aic(self):
        try:
            self.aic_garch = -2*self.l_garch + 2*len(self.theta_hat)
        except:
            pass
        
        self.aic = -2*self.llv + 2*len(self.theta_hat)

    def _calculate_bic(self):

        try:
            self.bic_garch = np.log(self.nobs)*len(self.theta_hat) - 2*self.l_garch
        except: 
            pass
        
        self.bic = np.log(self.nobs)*len(self.theta_hat) - 2*self.llv

    def residual_tests(self):

        # degrees of freedom model
        dof = len(self.theta_hat)
        # ljung box tests
        lb  = sm.stats.diagnostic.acorr_ljungbox(self.resid, model_df=dof, lags=[15], return_df = False)
        lb2 = sm.stats.diagnostic.acorr_ljungbox(self.resid**2, model_df=dof, lags=[15], return_df = False)
        # Kolmogorov-Smirnov
        if self.dist == 'Gaussian':
            ks = kstest(self.resid, norm.cdf)
        if self.dist == 'Student':
            idx = self.theta_names.index('vega')
            shape = self.theta_hat[idx]
            ks = kstest(self.resid, t.cdf, args=([shape]))
        if self.dist == 'GED':
            idx = self.theta_names.index('vega')
            shape = self.theta_hat[idx]
            ks = kstest(self.resid, gennorm.cdf, args=([shape]))
        if self.dist == 'SST':
            idx1 = self.theta_names.index('vega')
            idx2 = self.theta_names.index('vartheta')
            shape1 = self.theta_hat[idx1]
            shape2 = self.theta_hat[idx2]
            skew = SkewStudent(eta=shape2, lam=shape1)
            ks = kstest(self.resid, skew.cdf)

        self._dof = dof
        self._diag_names = ['LB', 'LB2', 'KS']
        self.diag_test_statistics = [lb[0][0], lb2[0][0], ks[0]]
        self.diag_pvals = [lb[1][0], lb2[1][0],  ks[1]]

##############
# BASE CLASS #
##############

class VolatilityModel(Metrics, Diagnostics):

    ''' Base class for volatility models '''

    def __init__(self, stock, split_date, dist, scale = True):

        # general variables
        self.stock = stock
        # used for model estimations
        # default
        if scale == True:
            self.scaler = 100
        else:
            self.scaler = 1

        self.stock_returns = (np.log(self.stock)*self.scaler).diff().dropna()
        self.mu = self.stock_returns.mean()

        # ret = stock returns demeaned and scaled
        self.ret = (self.stock_returns-self.mu) #* self.scaler

         # for train/test
        self.split_date = split_date
        self.x_train = self.ret[:self.split_date]
        self.nobs = len(self.x_train)
        self.x_test = self.ret[self.split_date:]

        # used for model estimations
        self.variance_ini = self.x_train.var() # sample variance
        self.dist = dist

    def _covariance_calculation(self):

        # used in set_estimation_values() to calculate SE (see kevin sheppard gjr)
        k = len(self.theta_hat)
        step = 1e-5 * self.theta_hat
        scores = np.zeros((self.nobs, k))
        for i in range(k):
            h = step[i]
            delta = np.zeros(k)
            delta[i] = h
            llvs_plus = self.log_likelihood(self.theta_hat + delta, output='SE')
            llvs_min  = self.log_likelihood(self.theta_hat - delta, output='SE')
            scores[:,i] = (llvs_plus - llvs_min)/(2*h)
        I = (scores.T @ scores)/self.nobs
        return I

    def set_estimation_values(self):

        # set new attributes! but first estimate a model
        self.theta_hat = self.result_optimization.x.copy()
        self.llv = - self.result_optimization.fun * self.nobs

        ###########################################################################
        # THIS SHOULD BE SOLVED / WRITTEN MORE CLEAN
        # most of cases this works
        try:
            self.variance = pd.Series(self.filtering(theta=self.theta_hat),
                                    index=self.x_train.index,
                                    name='conditional variance')

        # but not for realized garch......
        # because realized garch model filter returns 3 values.....
        # and log_h is 1 obs longer than x
        except:
            self.variance = pd.Series(np.exp(self.filtering(theta=self.theta_hat)[0]),
                                    index=self.x_train.index,
                                    name='conditional variance')
        ###########################################################################

        self.vola = np.sqrt(self.variance).rename('conditional volatility')
        self.resid = (self.x_train / self.vola).rename('fitted residuals')

        # standard errors etc.
        J = hessian_2sided(self.log_likelihood, self.theta_hat, 'hessian')
        I = self._covariance_calculation()
        J = J/self.nobs
        Jinv = np.mat(np.linalg.inv(J))
        vcv = Jinv*np.mat(I)*Jinv/self.nobs
        vcv = np.asarray(vcv)
        self.theta_se = np.sqrt(np.diag(vcv))
        self.tvals = self.theta_hat / self.theta_se
        self.pvals = norm.pdf(self.tvals)
        # self.theta_se = np.sqrt(np.diag( np.abs(np.linalg.inv(Hessian)) / self.nobs))
        # self.tvals = self.theta_hat / self.theta_se
        # self.pvals = norm.pdf(abs(self.tvals))

        # from diagnostics
        self._calculate_aic()
        self._calculate_bic()
        self.residual_tests()

    def show_estimation_values(self):

        # prints table with estimated values
        print('\n\n-----------------------------------------------')
        print(f'Estimation results from: \n{self.__str__}')
        print('-----------------------------------------------')
        print(f'LLV: {np.round(self.llv, 2)} | AIC: {np.round(self.aic, 2)} | BIC: {np.round(self.bic, 2)}')
        print('-----------------------------------------------')
        #print(f'Log-likelihood value: {self.llv}')
        print(tabulate({"Parameter": self.theta_names,
                        "Estimate" : self.theta_hat,
                        "Std. Err.": self.theta_se,
                        "T-stats"  : self.tvals}, headers="keys"))
        print('\n\n')

##############
# THE MODELS #
##############

class GARCH(VolatilityModel):

    ''' Model: GARCH(1,1) '''

    def __init__(self, stock, split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.__str__ = f'GARCH(1,1) with {self.dist} innovations'

        if dist == 'Gaussian':
            self.theta_names = ['omega', 'alpha', 'beta']
            self.theta_ini = np.array([0.05 , 0.05,  0.85])
        if dist == 'Student' or dist == 'GED':
            self.theta_names = ['omega', 'alpha', 'beta', 'vega']
            self.theta_ini = np.array([0.05 , 0.05,  0.9, 4])
        if dist == 'SST':
            self.theta_names = ['omega', 'alpha', 'beta', 'vega', 'vartheta']
            self.theta_ini = np.array([0.05 , 0.05,  0.9, 4, 1])

    def filtering(self, theta):

        ret = np.asarray(self.x_train)
        var = np.zeros(len(ret))
        var[0] = self.variance_ini
        # use model to filter
        for i in range(0, len(ret)-1):
            var[i+1] = theta[0] + theta[1]*ret[i]**2 + theta[2]*var[i]
        return var

    def log_likelihood(self, theta, output='optim'):

        ret = np.asarray(self.x_train)
        var = self.filtering(theta)
        l = np.empty(len(ret))
        # score to be optimized
        if self.dist == 'Gaussian':
            l = -(1/2)*np.log(2*np.pi) - (1/2)*np.log(var) - (1/2)*(ret**2)/var
        if self.dist == 'Student':
            v = theta[3]
            A = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi)) - np.log(gamma(v/2))
            l = A - ((v + 1)/2) * np.log(1 + ((ret**2/var) / v)) - np.log(np.sqrt(var))
        if self.dist == 'GED':
            v = theta[3]
            lmbd = (gamma(1/v)/(2**(2/v)*gamma(3/v)))**(1/2)
            l = -np.log(2**(1+(1/v))*gamma(1/v)*lmbd) - \
                (1/2)*np.log(var) + np.log(v) - (1/2)*np.abs(ret/(lmbd*var**(1/2)))**v
        if self.dist == 'SST':
            v = theta[3]
            r = theta[4]

            m = ( gamma((v-1)/2) / gamma(v/2) ) * np.sqrt(v/np.pi) * (r-1/r)
            s = np.sqrt( (r**2 + 1/r**2 - 1) - m**2 )
            for t in range(len(ret)):    
                temp = s*ret[t]/np.sqrt(var[t]) + m           
                if temp < 0:
                    ind = -1
                else:
                    ind = 1
                l[t] = np.log(gamma((v+1)/2)) - np.log(gamma(v/2)) - 0.5*np.log(v*np.pi*var[t]) \
                       + np.log(s) + np.log(2/(r+r**-1)) \
                       - (v+1)/2 * np.log( 1 + ((temp**2)/v)*r**(-2*temp) ) - (1/2)*np.log(var[t])

        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, mthd='SLSQP'):

        if self.dist == 'Gaussian':
            bnds = ((0.0000001, 1), (0, 1), (0, 1))
        if self.dist == 'Student' or self.dist == 'GED':
            bnds = ((0.0000001, 1), (0, 1), (0, 1), (0, 50))
        if self.dist == 'SST':
            bnds = ((0.0000001, 1), (0, 1), (0, 1), (0, 50), (0,100))

        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini,
                                            bounds=bnds, method=mthd)
        # check if succeeded
        if self.result_optimization.success == True:
            # from base class
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        theta = self.theta_hat
        ret = np.asarray(self.x_test)
        var = np.zeros(len(ret)+1)
        var[0] = self.variance[-1]
        # forecasts
        for i in range(0, len(ret)):
            var[i+1] = theta[0] + theta[1]*ret[i]**2 + theta[2]*var[i]
        # set values
        self.fct_variance = pd.Series(var[1:], index=self.x_test.index, name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')

class gjrGARCH(VolatilityModel):

    ''' Model: GJR_GARCH '''

    def __init__(self, stock, split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.__str__ = f'GJR-GARCH(1,1) with {self.dist} innovations'

        if dist == 'Gaussian':
            self.theta_names = ['omega', 'alpha', 'beta', 'gamma']
            self.theta_ini = np.array([0.05 , 0.05,  0.85, 0.1])
        if dist == 'Student' or dist == 'GED':
            self.theta_names = ['omega', 'alpha', 'beta', 'gamma', 'vega']
            self.theta_ini = np.array([0.05 , 0.05,  0.85, 0.1, 4])

    def filtering(self, theta):

        ret = np.asarray(self.x_train)
        var = np.zeros(len(ret))
        var[0] = self.variance_ini
        # use model to filter
        for i in range(0,len(ret)-1):
            var[i+1] = theta[0] + theta[1]*ret[i]**2 + theta[3] *ret[i]**2 * (ret[i]<0) + theta[2]*var[i]
        return var

    def log_likelihood(self, theta, output='optim'):

        ret = np.asarray(self.x_train)
        var = self.filtering(theta)
        # score to be optimized
        if self.dist == 'Gaussian':
            l = -(1/2)*np.log(2*np.pi) - (1/2)*np.log(var) - (1/2)*(ret**2)/var
        if self.dist == 'Student':
            v = theta[4]
            A = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi)) - np.log(gamma(v/2))
            l = A - ((v + 1)/2) * np.log(1 + ((ret**2/var) / v)) - np.log(np.sqrt(var))
        if self.dist == 'GED':
            v = theta[4]
            lmbd = (gamma(1/v)/(2**(2/v)*gamma(3/v)))**(1/2)
            l = -np.log(2**(1+(1/v))*gamma(1/v)*lmbd) - \
                (1/2)*np.log(var) + np.log(v) - (1/2)*np.abs(ret/(lmbd*var**(1/2)))**v

        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, mthd='SLSQP'):

        if self.dist == 'Gaussian':
            bnds = ((0.0000001, 100), (-1, 1), (0, 1), (0,1))
        if self.dist == 'Student' or self.dist == 'GED':
            bnds = ((0.0000001, 100), (-1, 1), (0, 1), (0,1), (0,50))

        def constraint(theta):
            alpha = theta[1]
            gamma = theta[3]
            beta = theta[2]
            return 1-alpha-gamma/2-beta
        constraints={"fun": constraint, 'type': 'ineq'}

        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini,
                                            bounds = bnds, method = mthd, constraints = constraints)
        if self.result_optimization.success == True:
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        theta = self.theta_hat
        ret = np.asarray(self.x_test)
        var = np.zeros(len(ret)+1)
        # use last fitted sigma and scale back
        var[0] = self.variance[-1]
        # forecasts
        for i in range(0, len(ret)):
            var[i+1] = theta[0] + theta[1]*ret[i]**2 + theta[3] *ret[i]**2 * (ret[i]<0) + theta[2]*var[i]
        # set values
        self.fct_variance = pd.Series(var[1:], index=self.x_test.index, name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')

class eGARCH(VolatilityModel):

    ''' Model: E-GARCH(1,1)'''

    def __init__(self, stock, split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.__str__ = f'E-GARCH(1,1) with {self.dist} innovations'

        if dist == 'Gaussian':
            self.theta_names = ['omega', 'alpha', 'beta', 'gamma']
            self.theta_ini = np.array([0.05, 0.05, 0.85, 0.05])
        if dist == 'Student' or dist == 'GED':
            self.theta_names = ['omega', 'alpha', 'beta', 'gamma', 'vega']
            self.theta_ini = np.array([0.05 , 0.05,  0.85, 0.05, 4])


    def filtering(self, theta):

        ret = np.asarray(self.x_train)
        log_h = np.zeros(len(ret))
        log_h[0] = np.log(self.variance_ini)
        # use model to filter
        if self.dist == 'Gaussian':
            for i in range(0, len(ret) - 1):
                log_h[i+1] = theta[0] + ( theta[3]*ret[i]) / np.sqrt(np.exp(log_h[i])) \
                             + theta[1]*( np.abs(ret[i]) / np.sqrt(np.exp(log_h[i])) - np.sqrt(2/np.pi) )  \
                             + theta[2]*log_h[i]
        if self.dist == 'Student':
            for i in range(0, len(ret) - 1):
                expectation = 2/np.sqrt(np.pi)*gamma((theta[4]+1)/2)/gamma(theta[4]/2)*np.sqrt(theta[4]-2)/(theta[4]-1)
                log_h[i+1] = theta[0] + ( theta[3]*ret[i])/ np.sqrt(np.exp(log_h[i])) + \
                    theta[1]* (np.abs(ret[i])/ np.sqrt(np.exp(log_h[i])) - expectation)  \
                        + theta[2]* log_h[i]
        if self.dist == 'GED':
            for i in range(0, len(ret) - 1):
                log_h[i+1] = theta[0] + ( theta[3]*ret[i]) / np.sqrt(np.exp(log_h[i])) \
                             + theta[1]*( np.abs(ret[i]) / np.sqrt(np.exp(log_h[i])) - np.sqrt(2/np.pi))  \
                             + theta[2]*log_h[i]
        return np.exp(log_h)

    def log_likelihood(self, theta, output='optim'):

        ret = np.asarray(self.x_train)
        var = self.filtering(theta)
        # score to be optimized
        if self.dist == 'Gaussian':
            l = -(1 / 2) * np.log(2 * np.pi) - (1 / 2) * np.log(var) - (1 / 2) * (ret ** 2) / var
        if self.dist == 'Student':
            v = theta[4]
            A = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi)) - np.log(gamma(v/2))
            l = A - ((v + 1)/2) * np.log(1 + ((ret**2/var) / v)) - np.log(np.sqrt(var))
        if self.dist == 'GED':
            v = theta[4]
            lmbd = (gamma(1/v)/(2**(2/v)*gamma(3/v)))**(1/2)
            l = -np.log(2**(1+(1/v))*gamma(1/v)*lmbd) - \
                (1/2)*np.log(var) + np.log(v) - (1/2)*np.abs(ret/(lmbd*var**(1/2)))**v

        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, dist='Gaussian', mthd='SLSQP'):

        if self.dist == 'Gaussian':
            bnds = ((0.0000001, 100), (0, 1), (0, 1), (-1,1))
        if self.dist == 'Student':
            bnds = ((0.0000001, 100), (0, 1), (0, 1), (-1,1), (0,50))
        if self.dist == 'GED':
            bnds = ((0.0000001, 1000), (-100, 100), (-100, 100), (-100,100), (0,500))
        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini,
                                            bounds = bnds, method=mthd)
        if self.result_optimization.success == True:
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        theta = self.theta_hat
        ret = np.asarray(self.x_test)
        log_h = np.zeros(len(ret) + 1)
        # use last fitted sigma and scale back
        log_h[0] = np.log(self.variance[-1])

        # forecasts
        if self.dist == 'Gaussian':
            for i in range(0, len(ret)):
                log_h[i+1] = theta[0] + ( theta[3]*ret[i]) / np.sqrt(np.exp(log_h[i])) \
                                 + theta[1]*( np.abs(ret[i]) / np.sqrt(np.exp(log_h[i])) - np.sqrt(2/np.pi) )  \
                                 + theta[2]*log_h[i]
        if self.dist == 'Student':
            for i in range(0, len(ret)):
                expectation = 2/np.sqrt(np.pi)*gamma((theta[4]+1)/2)/gamma(theta[4]/2)*np.sqrt(theta[4]-2)/(theta[4]-1)
                log_h[i+1] = theta[0] + ( theta[3]*ret[i])/ np.sqrt(np.exp(log_h[i])) + \
                    theta[1]* (np.abs(ret[i])/ np.sqrt(np.exp(log_h[i])) - expectation)  \
                        + theta[2]* log_h[i]
        if self.dist == 'GED':
            for i in range(0, len(ret)):
                log_h[i+1] = theta[0] + ( theta[3]*ret[i]) / np.sqrt(np.exp(log_h[i])) \
                             + theta[1]*( np.abs(ret[i]) / np.sqrt(np.exp(log_h[i])) - np.sqrt(2/np.pi))  \
                             + theta[2]*log_h[i]
        h = np.exp(log_h[1:])

        # set values
        self.fct_variance = pd.Series(h, index=self.x_test.index,
                                      name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')

class realGARCH(VolatilityModel):

    ''' Model: Realized GARCH(1,1) '''

    def __init__(self, stock, realized_variance, split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.__str__ = f'Log-linear Realized GARCH(1,1) with {self.dist} innovations'

        if dist == 'Gaussian':
            self.theta_names = ['omega', 'beta', 'gamma', 'ksi', 'phi', 'sigma_u', 'tau_1', 'tau_2']
            #self.theta_ini2 = [0.06, 0.55, 0.41, -0.18, 1.04, 0.38, -0.07, 0.07] -> 02C returns
            self.theta_ini = [0.18, 0.54, 0.43, -0.42, 0.99, 0.39, -0.11, 0.04]
        if dist == 'Student' or dist == 'GED':
            self.theta_names = ['omega', 'beta', 'gamma', 'ksi', 'phi', 'sigma_u', 'tau_1', 'tau_2', 'vega', 'vega1']
            self.theta_ini = [0.02, 0.6, 0.3, -0.15, 0.97, 0.61, 0.11, -0.04, 3, 3]

        # measure could be: {rv, bv, rk}
        self.realized_variance = realized_variance
        # for train/test
        self.rv_train = self.realized_variance[:self.split_date] * self.scaler**2
        self.rv_test = self.realized_variance[self.split_date:]  * self.scaler**2

    def filtering(self, theta):

        ret = np.asarray(self.x_train)
        log_h = np.zeros(len(ret))
        log_h[0] = np.log(np.var(ret))
        log_x = np.log(abs(np.asarray(self.rv_train)))
        # calculate values
        for i in range(0,len(ret)-1):
            log_h[i+1] = theta[0] + theta[1]*log_h[i] + theta[2]*log_x[i]
        #innovations
        z = ret / np.sqrt(np.exp(log_h))
        u = log_x - theta[3] - theta[4]*log_h - theta[6]*z**2 - theta[7]*(z**2 - 1)

        return log_h, z, u

    def log_likelihood(self, theta, output='optim'):

        ret = np.asarray(self.x_train)
        log_h, z, u = self.filtering(theta)

        # score to be optimized
        if self.dist == 'Gaussian':
            # garch part, normal gaussian
            l_ret = -0.5*(np.log(2*np.pi) + log_h + ret**2/np.exp(log_h))
            # u part, exactly the same as above but log_h = np.log(theta[5]**2) and ret = u
            l_u   = -0.5*(np.log(2*np.pi) + np.log(theta[5]**2) + u**2/theta[5]**2)

        if self.dist == 'Student':
            #first choose first vega for degrees of freedom
            v = theta[8]
            A_ret = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi)) - np.log(gamma(v/2))
            #use ret as ret and use np.exp(log_h) as var
            l_ret = A_ret - ((v + 1)/2) * np.log(1 + ((ret**2/np.exp(log_h)) / v)) - np.log(np.sqrt(np.exp(log_h)))
            #then choose second vega for degrees of freedom
            v = theta[9]
            A_u = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi)) - np.log(gamma(v/2))
            #use u as ret and use theta[5]**2 as var
            l_u = A_u - ((v + 1)/2) * np.log(1 + ((u**2/theta[5]**2) / v)) - np.log(np.sqrt(theta[5]**2))


        if self.dist == 'GED':
            #first choose first vega for degrees of freedom
            v = theta[8]
            lmbd_ret = (gamma(1/v)/(2**(2/v)*gamma(3/v)))**(1/2)
            #use ret as ret and use log_h as np.log(var) and np.exp(log_h) as var
            l_ret = -np.log(2**(1+(1/v))*gamma(1/v)*lmbd_ret) - \
                (1/2)*log_h + np.log(v) - (1/2)*np.abs(ret/(lmbd_ret*np.exp(log_h)**(1/2)))**v

            #then choose second vega for degrees of freedom
            v = theta[9]
            lmbd_u = (gamma(1/v)/(2**(2/v)*gamma(3/v)))**(1/2)
            #use u as ret and use theta[5]**2 as var and np.log(theta[5]**2) as np.log(var)
            l_u = -np.log(2**(1+(1/v))*gamma(1/v)*lmbd_u) - \
                (1/2)*np.log(theta[5]**2) + np.log(v) - (1/2)*np.abs(u/(lmbd_u*(theta[5]**2)**(1/2)))**v

        # split log likelihoods to compare garch models mutually
        ############################
        self.l_garch = np.sum(l_ret)
        self.l_u = np.sum(l_u)
        ############################

        l = l_ret + l_u

        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, dist='Gaussian', mthd='SLSQP', show_results=True):

        if self.dist == 'Gaussian':
            bnds =((0.0000001, 100), (0, 10), (0, 10), (-10, 10), (0, 10), (0, 10), (-10, 10), (-10, 10))
        if self.dist == 'Student' or self.dist == 'GED':
            bnds = ((-100, 100), (-100, 100), (-100, 100), (-20, 20), (-20, 20), (-20, 20), (-20, 20), (-20, 20), (0,100), (0,100))

        def constraints_function_real_garch(theta):
            beta = theta[1]
            gamma = theta[2]
            phi = theta[4]
            return 1-beta-(gamma*phi)
        constraints_real_garch = {"fun": constraints_function_real_garch, 'type': 'ineq'}

        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini,
                                            bounds = bnds, method = mthd, constraints = constraints_real_garch)
        if self.result_optimization.success == True:
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        theta = self.theta_hat
        ret = np.asarray(self.x_test)
        rv = np.asarray(self.rv_test)
        log_x = np.log(abs(rv))
        log_h = np.zeros(len(ret)+1)
        log_h[0] = np.log(self.variance[-1])
        # forecasts
        for i in range(0, len(ret)):
            log_h[i+1] = theta[0] + theta[1]*log_h[i] + theta[2]*log_x[i]
        h = np.exp(log_h)
        # set values
        self.fct_variance = pd.Series(h[1:], index=self.x_test.index, name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')

class GAS(VolatilityModel):

    '''GAS models'''

    def __init__(self, stock, split_date = None, dist = 'Gaussian'):
        super().__init__(stock, split_date, dist)
        self.__str__ = f'GAS(1,1) with {self.dist} innovations'

        # for filtering
        self.theta_hat = None

        if self.dist == 'Gaussian': 
            self.theta_names = ['omega', 'A', 'B']
            self.theta_ini = [0.005, 0.3, 0.8]
            self.bounds = ((0.00001, 10), (-100, 100),(-100, 100))

        if self.dist == 'Student': 
            self.theta_names = ['omega', 'A', 'B', 'vega']
            self.theta_ini = [0.005, 0.15, 0.99, 4]
            self.bounds = ((0.00001, 10), (-100, 100), (-100, 100), (2, 500))

        if self.dist == 'GED': 
            self.theta_names = ['omega', 'A', 'B', 'vega']
            self.theta_ini = [0.005, 0.15, 0.99, 4]
            self.bounds = ((-100, 100), (-100, 100), (-100, 100), (0, 5000))

    def filtering(self, theta):

        # x is demeaned
        ret = np.asarray(self.x_train)
        f = np.zeros(len(ret))
        f[0] = np.log(np.var(ret))

        for t in range(0, len(ret)-1):
            h = np.exp(f[t])

            if self.dist == 'Gaussian':
                score = -0.5 + (ret[t]**2) / (2*h)
            if self.dist == 'Student':
                v = theta[3]
                score = -0.5 + (v+1)/2 * (ret[t]**2) / (v*h + ret[t]**2) 
            if self.dist == 'GED':
                v = theta[3]
                lmb = ( gamma(1/v) / (2**(2/v) * gamma(3/v)) )**0.5
                score = -0.5/h + (np.abs(ret[t])**v)*v*((1/np.sqrt(h))**v) / (4*h*lmb**v)

            # the model
            f[t+1] = theta[0] + theta[1]*score + theta[2]*f[t]
        
        # for fitting 
        if self.theta_hat is None:
            return f
        else:
            return np.exp(f)

    def log_likelihood(self, theta, output = 'optim'):

        ret = np.asarray(self.x_train)
        f = self.filtering(theta)
        h = np.exp(f)

        if self.dist == 'Gaussian':
            l = -0.5*np.log(2*np.pi*h) - (ret**2) / (2*h)

        if self.dist == 'Student':
            v = theta[3]
            # formula from A.E.
            A = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi*h)) - np.log(gamma(v/2))
            l = A - ( (v+1)/2 )*np.log( 1 + (ret**2/(h*v)) ) 
            
        if self.dist == 'GED':
            v = theta[3]
            lmb = ( gamma(1/v) / (2**(2/v) * gamma(3/v)) )**0.5
            l = - np.log(2**(1+(1/v)) * gamma(1/v) * lmb) \
                    - 0.5*f + np.log(v) \
                    - 0.5*( np.abs(ret) / (lmb*(h**0.5)) )**v

        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, mthd ='SLSQP'):

        self.result_optimization = minimize(self.log_likelihood, self.theta_ini,
                                            method=mthd, bounds = self.bounds)
        self.success = self.result_optimization.success
         # check if succeeded
        if self.success == True:
            # from base class
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        theta = self.theta_hat
        ret = np.asarray(self.x_test)
        f = np.zeros(len(ret)+1)
        f[0] = np.log(self.variance[-1]) 
       
        for i in range(0, len(ret)):
            h = np.exp(f[i])

            if self.dist == 'Gaussian':
                score = -0.5 + (ret[i]**2) / (2*h)

            if self.dist == 'Student':
                v = theta[3]
                score = -0.5 + (v+1)/2 * (ret[i]**2) / (v*h + ret[i]**2) 

            if self.dist == 'GED':
                v = theta[3]
                lmb = ( gamma(1/v) / 2**(2/v)*gamma(3/v) )**0.5
                score = -0.5/h + (np.abs(ret[i])**v)*v*((1/np.sqrt(h))**v) / (4*h*lmb**v)
            
            f[i+1] = theta[0] + theta[1]*score + theta[2]*f[i]

        h = np.exp(f)
        # replace as setter to base class?
        self.fct_variance = pd.Series(h[1:], index=self.x_test.index, name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')

class realGAS(VolatilityModel):

    ''' Model: Realized GAS(1,1) '''

    def __init__(self, stock, realized_variance, split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.__str__ = f'Log-linear Realized GAS(1,1) with {self.dist} innovations'

        self.theta_hat = None
        if self.dist == 'Gaussian':
            self.theta_names = ['omega', 'A', 'B', 'zeta']
            self.theta_ini = [0.01, 0.1, 0.8, 1]
            self.bounds = ((-100, 100), (-100, 100),(-100, 100), (0,50))

        if self.dist == 'Student':
             self.theta_names = ['omega', 'A', 'B', 'zeta', 'vega']
             self.theta_ini = [0.01, 0.1, 0.6, 4, 7]
             self.bounds = ((-100, 100), (-100, 100),(-100, 100), (0,50), (0, 2000))

        if self.dist == 'GED':
             self.theta_names = ['omega', 'A', 'B', 'zeta', 'vega']
             self.theta_ini = [0.01, 0.2, 0.9, 4, 1.5]
             self.bounds = ((-100, 100), (-100, 100),(-100, 100), (0,50), (0, 200))

        # measure could be: {rv, bv, rk}
        self.realized_variance = realized_variance
        # for train/test
        self.rv_train = self.realized_variance[:self.split_date] * self.scaler**2
        self.rv_test = self.realized_variance[self.split_date:]  * self.scaler**2

    def filtering(self, theta):

        ret = np.asarray(self.x_train) 
        f = np.zeros(len(ret))
        f[0] = np.log(np.var(ret))
        rv = np.asarray(self.rv_train)
        z = theta[3]

        for t in range(0, len(ret)-1):
            h = np.exp(f[t])
            A = (z/2) * ((rv[t]/h) - 1)

            if self.dist == 'Gaussian':
                score = -0.5 + (ret[t]**2)/(2*h)

            if self.dist =='Student':
                v = theta[4]
                score = -0.5 + (v+1)/2 * (ret[t]**2) / (v*h + ret[t]**2) 

            if self.dist =='GED':
                v = theta[4]
                lmb = ( gamma(1/v) / (2**(2/v) * gamma(3/v)) )**0.5
                score = -0.5/h + (np.abs(ret[t])**v)*v*((1/np.sqrt(h))**v) / (4*h*lmb**v)

            s = A + score
            f[t+1] = theta[0] + theta[1]*s + theta[2]*f[t]

        if self.theta_hat is None:
            return f
        else:
            return np.exp(f)

    def log_likelihood(self, theta, output='optim'):

        ret = self.x_train
        f = self.filtering(theta) #log_h
        h = np.exp(f)
        rv = self.rv_train
        z = theta[3]

        # first term
        l_u = - np.log(gamma(z/2)) - (z/2)*np.log((2*h)/z) \
                  + (z/2 - 1)*np.log(rv) - (z*rv)/(2*h) 

        # second term
        if self.dist == 'Gaussian':
            #self.l_garch = -3584.85
            self.l_garch =  -2482.713
            l_ret = -0.5*( np.log(2*np.pi) + f + (ret**2)/h  )

        if self.dist == 'Student':
            #self.l_garch = -3583.6271
            self.l_garch = -2478.292
            v = theta[4]                  
            A = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi*h)) - np.log(gamma(v/2))
            l_ret = A - ( (v+1)/2 )*np.log( 1 + (ret**2/(h*v)) ) 

        if self.dist == 'GED':
            #self.l_garch = -3560.805
            self.l_garch = -2473.7910 
            v = theta[4]
            lmb = ( gamma(1/v) / (2**(2/v) * gamma(3/v)) )**0.5
            l_ret = - np.log(2**(1+(1/v)) * gamma(1/v) * lmb) \
                    - 0.5*f + np.log(v) \
                    - 0.5*( np.abs(ret) / (lmb*(h**0.5)) )**v        

        # split log likelihoods to compare garch models mutually
        ############################
        # self.l_garch = np.sum(l_ret)
        self.l_u = np.sum(l_u)
        ############################
        l = l_ret + l_u

        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, dist='Gaussian', mthd='SLSQP'):

        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini,
                                            bounds = self.bounds, method = mthd)
        if self.result_optimization.success == True:
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        theta = self.theta_hat
        ret = np.asarray(self.x_test)
        s = np.zeros(len(ret))
        f = np.zeros(len(ret)+1)
        f[0] = np.log(self.variance[-1])
        rv = np.asarray(self.rv_test)
        z = theta[3]

        for t in range(0, len(ret)):
            h = np.exp(f[t])
            A = (z/2) * ((rv[t]/h) - 1)

            if self.dist == 'Gaussian':
                score = -0.5 + (ret[t]**2) / (2*h)

            if self.dist =='Student':
                v = theta[4]
                score = -0.5 + (v+1)/2 * (ret[t]**2) / (v*h + ret[t]**2) 

            if self.dist == 'GED':
                v = theta[4]
                lmb = ( gamma(1/v) / 2**(2/v)*gamma(3/v) )**0.5
                score = -0.5/h + (np.abs(ret[t])**v)*v*((1/np.sqrt(h))**v) / (4*h*lmb**v)

            s = A + score
            f[t+1] = theta[0] + theta[1]*s + theta[2]*f[t]
        
        h = np.exp(f)
        self.fct_variance = pd.Series(h[1:], index=self.x_test.index, name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')

### extra model
class midasGARCH(VolatilityModel):

    '''
    Model: MIDAS_GARCH
    '''
    def __init__(self, stock, exog, low_freq='1M', K=6, split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.__str__ = f'MIDAS({K})-GARCH(1,1) with {exog.name} and {self.dist} innovations'

        self.theta_hat = None
        # some model specifications
        self.low_freq = low_freq
        self.K = K
        self.lags = np.arange(1, self.K+1)
        # the exogenous variable
        self.exog = exog
        # for train/test
        self.exog_train = self.exog[:self.split_date] # scale before inserting
        self.exog_test  = self.exog[self.split_date:] # no scaling required

        if self.dist == 'Gaussian':
            self.theta_names =  ['alpha', 'beta', 'm', 'zeta', 'w']
            self.theta_ini =  [0.1, 0.85, 0.001, 0.01, 0.5] # for gaussian
            self.theta_ini =  [0.1, 0.85, 0.01, 0.01, 1] # for t / ged
            self.bnds = ((0, 1), (0, 1), (0.00001, 10), (0.0001, 1), (0, 10))

    def _beta_weight_scheme(self, k, K, w1, w2=1):
        num = ((k/K)**(w1-1)) * (((1-k)/K)**(w2-1))
        denom = np.sum( [((j/K)**(w1-1)) * (((1-j)/K)**(w2-1)) for j in range(1, K+1)] )
        return num/denom

    def filtering(self, theta, ret=None, X=None):

        ''' 
        theta -> params to be optimized
        ret   -> returns daily
        X     -> exogenous variable monthly with daily index
        '''
        if ret == None and X == None:
            ret = self.x_train
            X = self.exog_train

        # unpack params
        alpha, beta, m, zeta, w = theta
        # make weeekly, monthly, quarterly, .. etc. index
        X_low_freq = X.resample(self.low_freq).last()
        # calculate weights for lags using beta weight function, take sum each loop to calculate tau
        tau = np.empty(X_low_freq.size)
        weights = [self._beta_weight_scheme(k, self.K, w) for k in self.lags]
        for t in range(X_low_freq.size):
            # lagged exogenous variables with appropriate weight coefficient
            terms = [weight*X_low_freq[t-k] if (t-k) >= 0 else 0 for k, weight in zip(self.lags, weights)] 
            tau[t] =  m + zeta*np.sum(terms)   
        tau = pd.Series(tau, name='tau', index=X_low_freq.index)
        df = pd.merge_asof(ret, tau, left_index=True, right_index=True, direction='forward')
        tau = df['tau']
        g = pd.Series(name='g', index=ret.index)
        g[0] = np.var(ret)
        # the model update equation
        for t in range(ret.size-1):
            g[t+1] = (1-alpha-beta) + alpha*((ret[t]**2)/tau[t]) + beta*g[t]

        if self.theta_hat is None:
            return g, tau
        else:
            return g*tau

    def log_likelihood(self, theta, output='optim', ret=None, X=None):

        if ret == None and X == None:
            ret = self.x_train
            X = self.exog_train

        try:
            g, tau = self.filtering(theta)
        except:
            g = self.filtering(theta)
            tau = 1
        
        if self.dist == 'Gaussian':
            l = -0.5*( np.log(2*np.pi) + np.log(g*tau) + (ret**2)/(g*tau) )
        
        if output == 'optim':
            self.short_run = g
            self.long_run = tau
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, mthd='SLSQP'):

        def constraint(theta): 
            return np.array(1-theta[0]-theta[1]) # alpha + beta < 1
        constraint={"fun": constraint, 'type': 'ineq'}
        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini, method=mthd,
                                            constraints=constraint, bounds=self.bnds)
        print(self.result_optimization.x)
        if self.result_optimization.success == True:
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):

        alpha, beta, m, zeta, w = self.theta_hat
        ret = self.x_test
        X = self.exog_test
        # make weekly, monthly, quarterly, .. etc. index
        X_low_freq = X.resample(self.low_freq).last()

        # do again for test set because we need this for 
        X_train = self.exog_train
        X_low_freq_train = X_train.resample(self.low_freq).last()

        # calculate weights for lags using beta weight function, take sum each loop to calculate tau
        tau = np.empty(X_low_freq.size)
        weights = [self._beta_weight_scheme(k, self.K, w) for k in self.lags]
        for t in range(X_low_freq.size):
            # lagged exogenous variables with appropriate weight coefficient
            terms = [weight*X_low_freq[t-k] if (t-k) >= 0 else weight*X_low_freq_train[t-k] \
                        for k, weight in zip(self.lags, weights)] 
            tau[t] =  m + zeta*np.sum(terms)   
        tau = pd.Series(tau, name='tau', index=X_low_freq.index)
        df = pd.merge_asof(ret, tau, left_index=True, right_index=True, direction='forward')
        tau = df['tau']
        g = pd.Series(name='g', index=ret.index)
        g[0] = self.variance[-1]
        # the model update equation
        for t in range(ret.size-1):
                g[t+1] = (1-alpha-beta) + alpha*((ret[t]**2)/tau[t]) + beta*g[t]

        self.fct_variance = pd.Series(g*tau[1:], index=self.x_test.index, name='predicted variance')
        self.fct_vola = np.sqrt(self.fct_variance).rename('predicted volatility')
        self.short_run = g
        self.long_run = tau