{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data cleaning"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy  as np\n",
    "\n",
    "pd.options.mode.chained_assignment = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> For convenience, this file is saved as a .html as it took really long to run all of it. Besides, the files that are loaded/imported aren't included in the repo for the same reason, simply took to long too push them..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "dtypes = {\n",
    "    \n",
    "    \"DATE\"  : str, \n",
    "    \"TIME\"  : str, \n",
    "    \"PRICE\" : str,\n",
    "    \"CORR\"  : str,\n",
    "    \"COND\"  : str,\n",
    "    \"EX\"    : str\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import and merge files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('data2007.txt', \n",
    "                 delimiter = r\"\\s+\", \n",
    "                 usecols   = [1,2,3,5,6,7], \n",
    "                 dtype     = dtypes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "years = np.arange(2008, 2015)\n",
    "\n",
    "for i in years:\n",
    "\n",
    "    temp = pd.read_csv(f'data{i}.txt', \n",
    "                       delimiter = r\"\\s+\", \n",
    "                       usecols   = [1,2,3,5,6,7], \n",
    "                       dtype     = dtypes)\n",
    "    \n",
    "    df = df.append(temp, ignore_index = True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>DATE</th>\n",
       "      <th>TIME</th>\n",
       "      <th>PRICE</th>\n",
       "      <th>CORR</th>\n",
       "      <th>COND</th>\n",
       "      <th>EX</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>20070503</td>\n",
       "      <td>9:32:26</td>\n",
       "      <td>21.7500</td>\n",
       "      <td>0</td>\n",
       "      <td>@</td>\n",
       "      <td>N</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>20070503</td>\n",
       "      <td>9:32:27</td>\n",
       "      <td>21.7500</td>\n",
       "      <td>0</td>\n",
       "      <td>@</td>\n",
       "      <td>D</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>20070503</td>\n",
       "      <td>9:32:27</td>\n",
       "      <td>21.7500</td>\n",
       "      <td>0</td>\n",
       "      <td>@</td>\n",
       "      <td>D</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>20070503</td>\n",
       "      <td>9:32:27</td>\n",
       "      <td>21.7500</td>\n",
       "      <td>0</td>\n",
       "      <td>@</td>\n",
       "      <td>D</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>20070503</td>\n",
       "      <td>9:32:27</td>\n",
       "      <td>21.7500</td>\n",
       "      <td>0</td>\n",
       "      <td>@</td>\n",
       "      <td>D</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>...</th>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "      <td>...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>83854410</th>\n",
       "      <td>20141231</td>\n",
       "      <td>19:00:32</td>\n",
       "      <td>49.1100</td>\n",
       "      <td>0</td>\n",
       "      <td>P</td>\n",
       "      <td>20</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>83854411</th>\n",
       "      <td>20141231</td>\n",
       "      <td>19:33:43</td>\n",
       "      <td>49.2000</td>\n",
       "      <td>0</td>\n",
       "      <td>T</td>\n",
       "      <td>K</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>83854412</th>\n",
       "      <td>20141231</td>\n",
       "      <td>19:55:41</td>\n",
       "      <td>49.2000</td>\n",
       "      <td>0</td>\n",
       "      <td>T</td>\n",
       "      <td>K</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>83854413</th>\n",
       "      <td>20141231</td>\n",
       "      <td>19:57:12</td>\n",
       "      <td>49.2000</td>\n",
       "      <td>0</td>\n",
       "      <td>T</td>\n",
       "      <td>K</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>83854414</th>\n",
       "      <td>20141231</td>\n",
       "      <td>19:58:52</td>\n",
       "      <td>49.2000</td>\n",
       "      <td>0</td>\n",
       "      <td>T</td>\n",
       "      <td>K</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "<p>83854415 rows × 6 columns</p>\n",
       "</div>"
      ],
      "text/plain": [
       "              DATE      TIME    PRICE CORR COND  EX\n",
       "0         20070503   9:32:26  21.7500    0    @   N\n",
       "1         20070503   9:32:27  21.7500    0    @   D\n",
       "2         20070503   9:32:27  21.7500    0    @   D\n",
       "3         20070503   9:32:27  21.7500    0    @   D\n",
       "4         20070503   9:32:27  21.7500    0    @   D\n",
       "...            ...       ...      ...  ...  ...  ..\n",
       "83854410  20141231  19:00:32  49.1100    0    P  20\n",
       "83854411  20141231  19:33:43  49.2000    0    T   K\n",
       "83854412  20141231  19:55:41  49.2000    0    T   K\n",
       "83854413  20141231  19:57:12  49.2000    0    T   K\n",
       "83854414  20141231  19:58:52  49.2000    0    T   K\n",
       "\n",
       "[83854415 rows x 6 columns]"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cleaning data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- See file from Bandorff, page C7"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**T1**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['0', '1', '12', '8', '10', 'CORR', '7', '11'], dtype=object)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.CORR.unique()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df[df.CORR == '0']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['0'], dtype=object)"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.CORR.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**T2**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['@', '5', 'E', 'F', 'Z', 'ZB', 'ZE', 'C', 'T', 'TE', 'TP', 'U',\n",
       "       'B', 'TB', '4', 'L', 'FZ', 'FT', 'O', 'P', 'UB', 'N', '4T', 'D',\n",
       "       'R', 'Q', 'M', 'R4', '6', 'X', 'N4', 'V', 'C4', 'K', 'J', 'I', 'Y',\n",
       "       'W'], dtype=object)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.COND.unique()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df[(df.COND == '@') | (df.COND == 'F') | (df.COND == 'E')] "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['@', 'E', 'F'], dtype=object)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.COND.unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**T3**\n",
    "\n",
    "*Not relevant to us*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**P2**\n",
    "\n",
    "*Not relevant to us*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**P3**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "D    20572746\n",
       "T    18188931\n",
       "P    12592803\n",
       "Z     8744067\n",
       "N     8720124\n",
       "K     4163551\n",
       "B     2889218\n",
       "J     2758600\n",
       "Y     2053229\n",
       "X      477163\n",
       "I      467254\n",
       "C      412267\n",
       "W      142828\n",
       "M       76651\n",
       "Name: EX, dtype: int64"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.EX.value_counts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df[df.EX == 'T'] # NASDAQ"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "T    18188931\n",
       "Name: EX, dtype: int64"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df.EX.value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's create a timestamp column for remaining df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "series = df.DATE + ' ' + df.TIME\n",
    "df['timestamp'] = pd.to_datetime(series, format = '%Y%m%d %H:%M:%S')\n",
    "df.set_index('timestamp', inplace = True)\n",
    "df.drop(['DATE', 'TIME'], inplace = True, axis = 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**P1**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df.between_time('9:30', '16:00')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**T4**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of taking median we could take weighted average using size??? But not required..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = df['PRICE']\n",
    "ts = ts.astype(float)\n",
    "ts = ts.resample('1S').median().dropna()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "timestamp\n",
       "2007-05-03 09:32:32    21.650\n",
       "2007-05-03 09:32:35    21.850\n",
       "2007-05-03 09:32:37    21.805\n",
       "2007-05-03 09:32:42    21.900\n",
       "2007-05-03 09:32:45    21.760\n",
       "                        ...  \n",
       "2014-12-31 15:59:45    49.170\n",
       "2014-12-31 15:59:48    49.180\n",
       "2014-12-31 15:59:51    49.185\n",
       "2014-12-31 15:59:52    49.180\n",
       "2014-12-31 15:59:55    49.180\n",
       "Name: PRICE, Length: 3631118, dtype: float64"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts.to_pickle('DAL_timeseries.pkl')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
