import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import math

from scipy.stats import norm, t, gennorm, kstest
from scipy.special import gammaln, gamma
from volatility_models import *

######################################################
######################################################

# ====================================== #
# ======= fit the models to date ======= #
# =======================================#
cutoff = '2012-12-31'

stock = pd.read_pickle('../data/AA_timeseries.pkl')
closing_prices = stock.resample('1D').agg('last').dropna()
opening_prices = stock.resample('1D').agg('first').dropna()

o2c_returns = np.log(closing_prices) - np.log(opening_prices)
c2o_returns = np.log(opening_prices)- np.log(closing_prices)
correction_factor = ( o2c_returns.var() + c2o_returns.var() ) / ( o2c_returns.var() )

# STILL HAS TO BE CORRECTED USING C2C-O2C / C2C   or something like that
var_measures = pd.read_pickle('../data/volatility_measures.pkl')
# since there were two negative estimated kernel values...
rk = np.abs(var_measures['rk'][1:]) * correction_factor

# for arch benchmarking
returns = (100 * np.log(closing_prices)).diff().dropna()[:cutoff]

# GARCH(1,1) GAUSSIAN
model1 = GARCH(closing_prices, dist='Gaussian', split_date=cutoff)
model1.fit()
model1.forecast()
model1.plot_vola()
model1.calculate_forecast_metrics(rk)

# GARCH(1,1) STUDENT
model2 = GARCH(closing_prices, dist='Student', split_date=cutoff)
model2.fit()
model2.forecast()
model2.plot_vola()
model2.calculate_forecast_metrics(rk)

#GARCH(1,1) GED
model3 = GARCH(closing_prices, dist='GED', split_date=cutoff)
model3.fit()
model3.forecast()
model3.plot_vola()
model3.calculate_forecast_metrics(rk)

#GJR-GARCH(1,1) GAUSSIAN
model4 = gjrGARCH(closing_prices, dist = 'Gaussian', split_date=cutoff)
model4.fit()
model4.forecast()
model4.plot_vola()
model4.calculate_forecast_metrics(rk)

#GJR-GARCH(1,1) STUDENT
model5 = gjrGARCH(closing_prices, dist = 'Student', split_date=cutoff)
model5.fit()
model5.forecast()
model5.plot_vola()
model5.calculate_forecast_metrics(rk)


#GJR-GARCH(1,1) STUDENT
model6 = gjrGARCH(closing_prices, dist = 'GED', split_date=cutoff)
model6.fit()
model6.forecast()
model6.plot_vola()
model6.calculate_forecast_metrics(rk)


# E-GARCH GAUSSIAN
model7 = eGARCH(closing_prices, dist = 'Gaussian', split_date=cutoff)
model7.fit()
model7.forecast()
model7.plot_vola()
model7.calculate_forecast_metrics(rk)

# E-GARCH STUDENT
model8 = eGARCH(closing_prices, dist = 'Student', split_date=cutoff)
model8.fit()
model8.forecast()
model8.plot_vola()
model8.calculate_forecast_metrics(rk)


# E-GARCH GED
model9 = eGARCH(closing_prices, dist = 'GED', split_date=cutoff)
model9.fit()
model9.forecast()
model9.plot_vola()
model9.calculate_forecast_metrics(rk)

#Realized GARCH(1,1) GAUSSIAN
model21 = realGARCH(closing_prices, rk, split_date=cutoff)
model21.fit()
model21.forecast()
model21.plot_vola()
model21.calculate_forecast_metrics(rk)

#Realized GARCH(1,1) STUDENT
model22 = realGARCH(closing_prices, rk,  dist = 'Student', split_date=cutoff)
model22.fit()
model22.forecast()
model22.plot_vola()
model22.calculate_forecast_metrics(rk)


#Realized GARCH(1,1) GED
model23 = realGARCH(closing_prices, rk,  dist = 'GED', split_date=cutoff)
model23.fit()
model23.forecast()
model23.plot_vola()
model23.calculate_forecast_metrics(rk)

# GETS SEPERATE TABLE

# GAS GAUSSIAN
model31 = GAS(closing_prices, dist = 'Gaussian', split_date=cutoff)
model31.fit()
model31.forecast()
model31.plot_vola()
model31.calculate_forecast_metrics(rk)

# GAS STUDENT
model32 = GAS(closing_prices, dist = 'Student', split_date=cutoff)
model32.fit()
model32.forecast()
model32.plot_vola()
model32.calculate_forecast_metrics(rk)

# GAS GED
model33 = GAS(closing_prices, dist = 'GED', split_date=cutoff)
model33.fit()
model33.forecast()
model33.plot_vola()
model33.calculate_forecast_metrics(rk)

#Realized GAS(1,1) GAUSSIAN
model34 = realGAS(closing_prices, rk, split_date=cutoff)
model34.fit()
model34.forecast()
model34.plot_vola()
model34.calculate_forecast_metrics(rk)

#Realized GAS(1,1) STUDENT
model35 = realGAS(closing_prices, rk,  dist = 'Student', split_date=cutoff)
model35.fit()
model35.forecast()
model35.plot_vola()
model35.calculate_forecast_metrics(rk)

#Realized GAS(1,1) GED
model36 = realGAS(closing_prices, rk,  dist = 'GED', split_date=cutoff)
model36.fit()
model36.forecast()
model36.plot_vola()
model36.calculate_forecast_metrics(rk)


##########
# TABLES #
##########

# significance levels
sl = [0.1, 0.05, 0.01]

###
def pretty_table_estimates(models, model_names, params, performance_metrics):

    # multi index
    dist = ['', '-t', '-GED']
    idx = [f'{m}{d}' for m in model_names for d in dist]
    midx = pd.MultiIndex.from_product([idx, ['coef', 'se']])
    cols = params + performance_metrics
    df = pd.DataFrame(index=midx, columns=cols)

    # fill table/df
    for m, i in zip(models, idx):

        try:
            df.loc[((i), 'coef'), 'llv'] = m.l_garch
            df.loc[((i), 'coef'), 'aic'] = m.aic_garch
            df.loc[((i), 'coef'), 'bic'] = m.bic_garch
            # df.loc[((i), 'se'), 'llv'] = m.llv
            # df.loc[((i), 'se'), 'aic'] = m.aic
            # df.loc[((i), 'se'), 'bic'] = m.bic

        except:
            df.loc[((i), 'coef'), 'llv'] = m.llv
            df.loc[((i), 'coef'), 'aic'] = m.aic
            df.loc[((i), 'coef'), 'bic'] = m.bic
        
        for name, coef, se, p in zip(m.theta_names, m.theta_hat, m.theta_se, m.pvals):
            df.loc[((i), 'coef'), name] = np.round(coef, 3)
            se = np.round(se, 2)
            if p <= sl[2] and isinstance(p, float):
                df.loc[((i),'se'), name] = f'({se})***'
            elif p <= sl[1] and isinstance(p, float):
                df.loc[((i),'se'), name] = f'({se})**'
            elif p <= sl[0] and isinstance(p, float):
                df.loc[((i),'se'), name] = f'({se})*'
            else:
                df.loc[((i),'se'), name] = f'({se})'

    return df.fillna('')

# first pretty table
names1 = ['GARCH', 'GJR-GARCH', 'E-GARCH', 'Realized-GARCH']#, 'GAS', 'Robust-Leveraged GARCH', 'MIDAS-GARCH']
param1 = ['omega', 'alpha', 'beta', 'gamma', 'vega' ,'vega1' ,'ksi', 'phi', 'sigma_u', 'tau_1', 'tau_2'] # rho # delta
prfrm1 =  ['llv', 'aic', 'bic']
models1 = [model1, model2, model3, # garch
           model4, model5, model6, # gjrgarch
           model7, model8, model9, # egarch
           model21, model22, model23] # realized garch

df_table_GARCH = pretty_table_estimates(models1, names1, param1, prfrm1)

names2 = ['GAS', 'Realized-GAS']
param2 = ['omega', 'A', 'B', 'zeta', 'vega']
prfrm2 = ['llv', 'bic', 'aic']
models2 = [model31, model32, model33,
           model34, model35, model36]

df_table_GAS = pretty_table_estimates(models2, names2, param2, prfrm2)

###
def pretty_table_diagnostics(models, model_names):

    dist = ['', '-t', '-GED']
    idx = [f'{m}{d}' for m in model_names for d in dist]
    midx = pd.MultiIndex.from_product([idx, ['test', 'p']])
    cols = ['DoF', 'LB', 'LB2', 'KS']
    df = pd.DataFrame(index=midx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[((i), 'test'), 'DoF'] = m._dof
        except:
            pass

        try:
            for name, test, p in zip(m._diag_names, m.diag_test_statistics, m.diag_pvals):
                try:
                    test = np.round(test, 2)
                    if p <= sl[2] and isinstance(p, float):
                        df.loc[((i),'test'), name] = f'{test}***'
                    elif p <= sl[1] and isinstance(p, float):
                        df.loc[((i),'test'), name] = f'{test}**'
                    elif p <= sl[0] and isinstance(p, float):
                        df.loc[((i),'test'), name] = f'{test}*'
                    else:
                        df.loc[((i),'test'), name] = f'{test}'
                except:
                    pass
        except:
            pass

    return df.loc[(slice(None), 'test'), :].reset_index(1, drop=True)

names = names1 + names2 
models = models1 + models2

df_table_diag = pretty_table_diagnostics(models, names)

###
from dm_test import *
def pretty_table_diebold(models, names, true_variance, method):

    # multi index
    dist = ['', '-t', '-GED']
    all_models = [f'{m}{d}' for m in names for d in dist]
    df = pd.DataFrame(columns = all_models, index = all_models)

    for m1, name1 in zip(models, all_models):
        for m2, name2 in zip(models, all_models):
            if m1 == m2:
                pass
            else:
                try:
                    true = np.asarray(true_variance)
                    pred1 = np.asarray(m1.fct_variance)
                    pred2 = np.asarray(m2.fct_variance)

                    dm = dm_test(true, pred1, pred2, h=1, crit=method)
                    test_stat = np.round(dm[0], 2)
                    p = dm[1]

                    # if p <= sl[2] and isinstance(p, float):
                    #     df.loc[name1, name2] = f'{test_stat}***'
                    if p <= sl[1] and isinstance(p, float):
                        df.loc[name1, name2] = f'{test_stat}*'
                    # elif p <= sl[0] and isinstance(p, float):
                    #     df.loc[name1, name2] = f'{test_stat}*'
                    else:
                        df.loc[name1, name2] = f'{test_stat}'
                except:
                    pass

    return df.fillna('-')

true_variance = rk[cutoff:] * 100**2

df_dm_fmae = pretty_table_diebold(models, names, true_variance, 'MAD')
df_dm_fmse = pretty_table_diebold(models, names, true_variance, 'MSE')

###
def pretty_table_fmetrics(models, names):

    dist = ['', '-t', '-GED']
    idx = [f'{m}{d}' for m in names for d in dist]
    cols = ['FMSE', 'FMAE']
    df = pd.DataFrame(index=idx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[i, 'FMAE'] = f'{m.fmae:.4f}'
            df.loc[i, 'FMSE'] = f'{m.fmae:.4f}'
        except:
            pass

    return df.fillna('')

df_fmetrics = pretty_table_fmetrics(models, names)

###
def pretty_table_VaR_backtest(models, names):

    dist = ['', '-t', '-GED']
    idx = [f'{m}{d}' for m in names for d in dist]
    cols = ['90%', '95%', '99%']
    df = pd.DataFrame(index=idx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[i, '90%'] = f'{m.coverage_VaR[0]*100:.2f}'
            df.loc[i, '95%'] = f'{m.coverage_VaR[1]*100:.2f}'
            df.loc[i, '99%'] = f'{m.coverage_VaR[2]*100:.2f}'
        except:
            pass

    return df.fillna('')

df_var = pretty_table_VaR_backtest(models, names)

###
def table_in_out_sample_variances(models, names):

    dist = ['', '-t', '-GED']
    idx = [f'{m}{d}' for m in names for d in dist]
    cols = ['train', 'test']
    df = pd.DataFrame(index=idx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[i, 'train'] = np.round(m.variance.mean(), 2)
            df.loc[i, 'test']  = np.round(m.fct_variance.mean(), 2)
        except:
            pass

    return df.fillna('')

df = table_in_out_sample_variances(models, names)
