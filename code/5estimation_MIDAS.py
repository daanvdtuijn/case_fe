import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.optimize import fmin_slsqp
from volatility_models import *

cutoff = '2012-12-31'

### NASDAQ
df = pd.read_pickle('../data/IXIC_volas.pkl')
df.index = pd.to_datetime(df.index)
closing_prices = df['close_price']

####### RK
rk_daily = df['rk_parzen']*2 # correction factor

rk = rk_daily.resample('1M').sum().resample('B').fillna('bfill')
closing_prices = closing_prices.loc[rk.index[0]:]
rk = rk[closing_prices.index]
rk = rk[1:]

model_rk = midasGARCH(closing_prices, rk, split_date=cutoff)
model_rk.fit()
model_rk.forecast()
model_rk.plot_vola()
model_rk.calculate_forecast_metrics(rk_daily)

###### EUR/USD
eurusd = pd.read_pickle('../data/eurusd.pkl')

model_eurusd = midasGARCH(closing_prices, eurusd, split_date=cutoff)
model_eurusd.fit()
model_eurusd.forecast()
model_eurusd.plot_vola()
model_eurusd.calculate_forecast_metrics(rk_daily)

###### GT
google = pd.read_pickle('../data/google_trends.pkl')
google.resample('1M').sum().resample('B').fillna('bfill')

model_google = midasGARCH(closing_prices, google, split_date=cutoff)
model_google.fit()
model_google.forecast()
model_google.plot_vola()
model_google.calculate_forecast_metrics(rk_daily)
 
###### TABLES

sl = [0.1, 0.05, 0.01]

def pretty_table_estimates(models, model_names, params, performance_metrics):

    # multi index
    dist = [' RK', ' EUR/USD', ' GT']
    idx = [f'{m}{d}' for m in model_names for d in dist]
    midx = pd.MultiIndex.from_product([idx, ['coef', 'se']])
    cols = params + performance_metrics
    df = pd.DataFrame(index=midx, columns=cols)

    # fill table/df
    for m, i in zip(models, idx):

        try:
            df.loc[((i), 'coef'), 'llv'] = m.l_garch
            df.loc[((i), 'coef'), 'aic'] = m.aic_garch
            df.loc[((i), 'coef'), 'bic'] = m.bic_garch
            # df.loc[((i), 'se'), 'llv'] = m.llv
            # df.loc[((i), 'se'), 'aic'] = m.aic
            # df.loc[((i), 'se'), 'bic'] = m.bic

        except:
            df.loc[((i), 'coef'), 'llv'] = m.llv
            df.loc[((i), 'coef'), 'aic'] = m.aic
            df.loc[((i), 'coef'), 'bic'] = m.bic
        
        for name, coef, se, p in zip(m.theta_names, m.theta_hat, m.theta_se, m.pvals):
            df.loc[((i), 'coef'), name] = np.round(coef, 3)
            se = np.round(se, 2)
            if p <= sl[2] and isinstance(p, float):
                df.loc[((i),'se'), name] = f'({se})***'
            elif p <= sl[1] and isinstance(p, float):
                df.loc[((i),'se'), name] = f'({se})**'
            elif p <= sl[0] and isinstance(p, float):
                df.loc[((i),'se'), name] = f'({se})*'
            else:
                df.loc[((i),'se'), name] = f'({se})'

    return df.fillna('')

# first pretty table
names = ['MIDAS-GARCH']
param = ['alpha', 'beta', 'm', 'zeta' ,'w']
prfrm =  ['llv', 'aic', 'bic']
models = [model_rk, model_eurusd, model_google]

df_table_GARCH = pretty_table_estimates(models, names, param, prfrm)

def pretty_table_diagnostics(models, model_names):

    dist = [' RK', ' EUR/USD', ' GT']
    idx = [f'{m}{d}' for m in model_names for d in dist]
    midx = pd.MultiIndex.from_product([idx, ['test', 'p']])
    cols = ['DoF', 'LB', 'LB2', 'KS']
    df = pd.DataFrame(index=midx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[((i), 'test'), 'DoF'] = m._dof
        except:
            pass

        try:
            for name, test, p in zip(m._diag_names, m.diag_test_statistics, m.diag_pvals):
                try:
                    test = np.round(test, 2)
                    if p <= sl[2] and isinstance(p, float):
                        df.loc[((i),'test'), name] = f'{test}***'
                    elif p <= sl[1] and isinstance(p, float):
                        df.loc[((i),'test'), name] = f'{test}**'
                    elif p <= sl[0] and isinstance(p, float):
                        df.loc[((i),'test'), name] = f'{test}*'
                    else:
                        df.loc[((i),'test'), name] = f'{test}'
                except:
                    pass
        except:
            pass

    return df.loc[(slice(None), 'test'), :].reset_index(1, drop=True)

df_table_diag = pretty_table_diagnostics(models, names)

from dm_test import *
def pretty_table_diebold(models, names, true_variance, method):

    # multi index
    dist = [' RK', ' EUR/USD', ' GT']
    all_models = [f'{m}{d}' for m in names for d in dist]
    df = pd.DataFrame(columns = all_models, index = all_models)

    for m1, name1 in zip(models, all_models):
        for m2, name2 in zip(models, all_models):
            if m1 == m2:
                pass
            else:
                try:
                    true = np.asarray(true_variance[1:])
                    pred1 = np.asarray(m1.fct_variance.dropna())
                    pred2 = np.asarray(m2.fct_variance.dropna())

                    dm = dm_test(true, pred1, pred2, h=1, crit=method)
                    test_stat = np.round(dm[0], 2)
                    p = dm[1]

                    # if p <= sl[2] and isinstance(p, float):
                    #     df.loc[name1, name2] = f'{test_stat}***'
                    if p <= sl[1] and isinstance(p, float):
                        df.loc[name1, name2] = f'{test_stat}*'
                    # elif p <= sl[0] and isinstance(p, float):
                    #     df.loc[name1, name2] = f'{test_stat}*'
                    else:
                        df.loc[name1, name2] = f'{test_stat}'
                except:
                    pass

    return df.fillna('-')

df = pd.read_pickle('../data/IXIC_volas.pkl')
df.index = pd.to_datetime(df.index)
closing_prices = df['close_price']
opening_prices = df['open_price']
o2c_returns = np.log(closing_prices) - np.log(opening_prices)
c2o_returns = np.log(opening_prices)- np.log(closing_prices)
correction_factor = ( o2c_returns.var() + c2o_returns.var() ) / ( o2c_returns.var() )
rk = np.abs(df['rk_parzen'][1:]) * correction_factor
true_variance = rk[cutoff:] * 100**2

df_dm_fmae = pretty_table_diebold(models, names, true_variance, 'MAD')
df_dm_fmse = pretty_table_diebold(models, names, true_variance, 'MSE')

dm_test(true_variance[1:], model_rk.fct_variance.dropna(), model_eurusd.fct_variance.dropna(), h=1, crit='MSE')

def pretty_table_fmetrics(models, names):

    dist = [' RK', ' EUR/USD', ' GT']
    idx = [f'{m}{d}' for m in names for d in dist]
    cols = ['FMSE', 'FMAE']
    df = pd.DataFrame(index=idx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[i, 'FMAE'] = f'{m.fmae:.4f}'
            df.loc[i, 'FMSE'] = f'{m.fmse:.4f}'
        except:
            pass

    return df.fillna('')

df_fmetrics = pretty_table_fmetrics(models, names)

def pretty_table_VaR_backtest(models, names):

    dist = [' RK', ' EUR/USD', ' GT']
    idx = [f'{m}{d}' for m in names for d in dist]
    cols = ['90%', '95%', '99%']
    df = pd.DataFrame(index=idx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[i, '90%'] = f'{m.coverage_VaR[0]*100:.2f}'
            df.loc[i, '95%'] = f'{m.coverage_VaR[1]*100:.2f}'
            df.loc[i, '99%'] = f'{m.coverage_VaR[2]*100:.2f}'
        except:
            pass

    return df.fillna('')

df_var = pretty_table_VaR_backtest(models, names)


###
def table_in_out_sample_variances(models, names):

    dist = [' RK', 'EUR/USD', ' GT']
    idx = [f'{m}{d}' for m in names for d in dist]
    cols = ['train', 'test']
    df = pd.DataFrame(index=idx, columns=cols)

    for m, i in zip(models, idx):
        try:
            df.loc[i, 'train'] = np.round(m.variance.mean(), 2)
            df.loc[i, 'test']  = np.round(m.fct_variance.mean(), 2)
        except:
            pass

    return df.fillna('')

df = table_in_out_sample_variances(models, names)




# true = np.log(closing_prices[cutoff:]).diff().dropna()*100

# model_rk.fct_vola.to_pickle('../data/fct_midas.pkl')

# model_rk.plot_vola()

# model_rk.calculate_forecast_metrics(rk)

