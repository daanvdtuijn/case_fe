from matplotlib.gridspec import GridSpec
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np 
import statsmodels.api as sm
import matplotlib
import seaborn as sns
import datetime as dt
import scipy.stats as stats
import math

matplotlib.rcParams['font.family'] = "serif"

cutoff = '2012-12-31'

stock = pd.read_pickle('../data/AA_timeseries.pkl')
closing_prices = stock.resample('1D').agg('last').dropna()
opening_prices = stock.resample('1D').agg('first').dropna()

o2c_returns = np.log(closing_prices) - np.log(opening_prices)
c2o_returns = np.log(opening_prices)- np.log(closing_prices)
correction_factor = ( o2c_returns.var() + c2o_returns.var() ) / ( o2c_returns.var() )

var_measures = pd.read_pickle('../data/volatility_measures.pkl')
rk = np.abs(var_measures['rk'][1:]) * correction_factor

# returns
returns = (100 * np.log(closing_prices)).diff().dropna()

# global formtters
FmtYM = mdates.DateFormatter('%Y-%m')
FmtY  = mdates.DateFormatter('%Y')

############
# measures #
############

measures = np.abs(pd.read_pickle('../data/volatility_measures.pkl'))
measures = measures[['bv', 'rv', 'rk']]
measures.rename(columns={'rv': 'Realized variance', 'bv': 'Bipower variance', 'rk': 'Realized kernel'}, 
                inplace=True)

####################################################
df_nsdq = pd.read_pickle('../data/IXIC_volas.pkl')
measures_nasdaq = df_nsdq[['bv', 'rv5', 'rk_parzen']]
measures_nasdaq.rename(columns={'rk_parzen': 'Realized kernel', 'bv': 'Bipower variance', 'rv5': 'Realized variance'},
                inplace=True)
measures_nasdaq.index = pd.to_datetime(measures_nasdaq.index)
####################################################

def plot_measures(measures, ylim=False):

    fig=plt.figure(figsize=(10,6))
    gs=GridSpec(2,5) # 2 rows, 3 columns

    ax1=fig.add_subplot(gs[0,:]) # First row, first column
    ax2=fig.add_subplot(gs[1,0:2]) # First row, second column
    ax3=fig.add_subplot(gs[1,2:]) # First row, third column

    measures.plot(ax=ax1, lw=.7)
    ax1.set_xlabel('')
    ax1.axvspan('2008-9','2009-6', alpha=0.6, color='lightgrey')
    ax1.axvspan('2013-9','2015-01', alpha=0.6, color='lightgrey')
    if ylim == True:
        ax1.set_ylim([-0.001, 0.02])
    ax1.axvline('2013-9', color='grey', ls='--', label='Train/test split')
    ax1.legend(loc='upper center', ncol=2, frameon=False)
    ax1.tick_params(axis='x', labelrotation=0)
    ax1.set_title('Entire sample')

    measures['2008-9-1':'2009-6-1'].plot(ax=ax2, lw=.7)
    ax2.set_xlabel('')
    ax2.get_xaxis().set_ticks([])
    ax2.set_xticks(['2008-9', '2008-12', '2009-3', '2009-6'])
    ax2.xaxis.set_major_formatter(FmtYM)
    ax2.get_legend().remove()
    ax2.set_title('Crisis period')
    ax2.tick_params(axis='x', labelrotation=0)

    measures[cutoff:].plot(ax=ax3, lw=.7)
    ax3.set_xlabel('')
    ax3.set_xticks(['2013-01', '2013-07', '2014-01', '2014-07', '2015-01'])
    ax3.xaxis.set_major_formatter(FmtYM)
    ax3.get_legend().remove()
    ax3.set_title('Validation sample')
    ax3.tick_params(axis='x', labelrotation=0)

    fig.tight_layout()
    
fig = plot_measures(measures, ylim=True);
fig = plot_measures(measures_nasdaq);
#fig.savefig('../images/measures_index.png')

########
# data #
########


####################################################

closing_prices_nasdaq = df_nsdq['close_price']
closing_prices_nasdaq.index = pd.to_datetime(closing_prices_nasdaq.index)
returns_nasdaq = (100 * np.log(closing_prices_nasdaq)).diff().dropna()

#############################################
# #######


def plot_data_one(closing_prices, returns):

    # first
    fig = plt.figure(figsize=(10,3))
    gs=GridSpec(1,2) 

    ax1=fig.add_subplot(gs[0,0]) # First row, first column
    ax2=fig.add_subplot(gs[0,1]) # First row, second column

    closing_prices[:cutoff].plot(ax=ax1, c='C2', label='Train')
    closing_prices[cutoff:].plot(ax=ax1, c='C1', label='Test')
    ax1.set_title('Price')
    ax1.set_xticks(['2007', '2009', '2011', '2013', '2015'])
    ax1.xaxis.set_major_formatter(FmtY)
    ax1.tick_params(axis='x', labelrotation=0)
    ax1.set_xlabel('')
    ax1.legend(loc='upper left', ncol=2, frameon=False)

    returns[:cutoff].plot(ax=ax2, color='C2', label='')
    returns[cutoff:].plot(ax=ax2, color='C1', label='')
    returns.rolling(window=20).std().plot(color='C0', lw=.8, label='20 day rolling std.')
    ax2.set_title('Logarithmic returns (x100)')
    ax2.legend(loc='upper right', frameon=False)
    ax2.set_xticks(['2007', '2009', '2011', '2013', '2015'])
    ax2.xaxis.set_major_formatter(FmtY)
    ax2.tick_params(axis='x', labelrotation=0)
    ax2.set_xlabel('')

    fig.tight_layout()
    return fig

fig = plot_data_one(closing_prices, returns);
fig = plot_data_one(closing_prices_nasdaq, returns_nasdaq);


def plot_data_two(returns):

    # second
    fig = plt.figure(figsize=(10,3))
    gs=GridSpec(1,2) 

    ax1=fig.add_subplot(gs[0,0]) # First row, first column
    ax2=fig.add_subplot(gs[0,1]) # First row, second column

    sns.kdeplot(data=returns, label='KDE', ax=ax1, color='C1')
    ax1.set_title('Distribution of returns')
    ax1.grid(False)
    ax1.legend(loc='upper left', frameon=False)
    ax1.yaxis.label.set_color('C1')
    ax1.tick_params(axis='y', colors='C1')
    ax1.set_xlabel(' ')
    ax1.set_ylabel('Density', color='C1')

    ax11 = ax1.twinx()
    returns.hist(bins=80,ax=ax11, color='C2', edgecolor='C1', label='Observed')
    ax11.grid(False)
    ax11.legend(frameon=False)
    ax11.yaxis.label.set_color('C2')
    ax11.tick_params(axis='y', colors='C2')

    sm.qqplot(returns, fit=True, ax=ax2, line='45')
    ax2.set_title('QQ plot Gaussian')
    ax2.get_lines()[0].set_color('C2')
    ax2.get_lines()[0].set_markersize(.9)
    ax2.get_lines()[1].set_color('C1')
    ax2.set_xlabel('Sample')
    ax2.set_ylabel('Observed')

    fig.tight_layout()
    return fig

fig = plot_data_two(returns);
fig = plot_data_two(returns_nasdaq);

# df = pd.read_csv('../data/oxfordmanrealizedvolatilityindices.csv', index_col=[0,1])
# df = df.loc[ (slice(None), '.IXIC'), :]
# df.reset_index(level=1, inplace=True)
# df.index = [x.date() for x in pd.to_datetime(df.index)]
# df = df.loc[closing_prices.index[0].date():closing_prices.index[-1].date()]
# df = df[['bv', 'rv5', 'rk_parzen', 'rk_twoscale', 'open_to_close', 'open_price', 'close_price']]
# df.to_pickle('../data/IXIC_volas.pkl')


forecasts = pd.read_pickle('plot_fct.pkl')
forecasts_nasdaq = pd.read_pickle('../data/plot_fct_nasdaq.pkl')

def plot_forecasts(forecasts, forecasts2):

    fig = plt.figure(figsize=(10,3))
    gs=GridSpec(1,2) 

    ax1=fig.add_subplot(gs[0,0]) # First row, first column
    ax2=fig.add_subplot(gs[0,1]) # First row, second column

    ax1.plot(np.sqrt(forecasts['true']), alpha=.9)
    np.sqrt(forecasts).iloc[:,2:0:-1].plot(ax=ax1, rot=0,lw=.9)
    ax1.legend(frameon=False)
    ax1.set_ylabel('Volatility')
    ax1.set_xlabel('')
    ax1.xaxis.set_major_formatter(FmtY)
    ax1.set_xticks(['2013', '2014', '2015'])
    ax1.set_title('Alcoa Corp.')


    np.sqrt(forecasts_nasdaq).iloc[:,1:].plot(ax=ax2, color=['C1', 'C2'], rot=0,lw=.9)
    ax2.plot(np.sqrt(forecasts_nasdaq['true']), label='Actual - Realized kernel', zorder=1, alpha=.9)
    ax2.legend(frameon=False, ncol=1)
    ax2.xaxis.set_major_formatter(FmtY)
    ax2.set_xticks(['2013', '2014', '2015'])
    ax2.set_title('NASDAQ')

    fig.tight_layout()
    return fig


fig = plot_forecasts(forecasts, forecasts_nasdaq)
fig.savefig('../images/volatility_fct.png')

VaRs = pd.read_pickle('plot_var.pkl')
VaRs_nasdaq = pd.read_pickle('plot_var_nasdaq.pkl')

def plot_vars(VaRs, VaRs_nasdaq):

    fig = plt.figure(figsize=(10,3))
    gs=GridSpec(1,2) 

    ax1=fig.add_subplot(gs[0,0]) # First row, first column
    ax2=fig.add_subplot(gs[0,1]) # First row, second column

    ax1.plot(VaRs.iloc[:,0], label='Actual')
    ax1.plot(VaRs.iloc[:,1],lw=.7)
    ax1.plot(VaRs.iloc[:,2],lw=.7)
    ax1.plot(VaRs.iloc[:,3],lw=.7)
    ax1.legend(frameon=False,ncol=2)
    ax1.set_ylabel('Returns')
    ax1.set_xlabel('')
    ax1.xaxis.set_major_formatter(FmtY)
    ax1.set_xticks(['2013', '2014', '2015'])
    ax1.set_title('Alcoa Corp. - GAS-GED')

    ax2.plot(VaRs_nasdaq.iloc[:,0])
    ax2.plot(VaRs_nasdaq.iloc[:,1], label='90%',lw=.7)
    ax2.plot(VaRs_nasdaq.iloc[:,2], label='95%', lw=.7)
    ax2.plot(VaRs_nasdaq.iloc[:,3], label='99%',lw=.7)
    ax2.legend(frameon=False, loc='lower left', ncol=1)
    ax2.set_xlabel('')
    ax2.xaxis.set_major_formatter(FmtY)
    ax2.set_xticks(['2013', '2014', '2015'])
    ax2.set_title('NASDAQ - LLR-GARCH-GED')

    fig.tight_layout()
    return fig

fig = plot_vars(VaRs, VaRs_nasdaq)
fig.savefig('../images/vars_plot.png')


################ distributions

mu = 0
variance = 1
sigma = math.sqrt(variance)
x = np.linspace(mu - 4*sigma, mu + 4*sigma, 100)

fig, ax = plt.subplots(figsize=(9,4))
ax.plot(x, stats.norm.pdf(x, mu, sigma), label='Gaussian',lw=.8)
ax.plot(x, stats.t.pdf(x, 6), label= r'Student-T with $\nu =6$',lw=.8)
ax.plot(x, stats.gennorm.pdf(x, 1.4), label=r'Generalized Error with $\nu = 1.5$',lw=.8)
ax.fill(x, stats.norm.pdf(x, mu, sigma),  alpha=.2) 
ax.fill(x, stats.t.pdf(x, 6), alpha=.2)
ax.fill(x, stats.gennorm.pdf(x, 1.4), alpha=.1)
ax.set_title('')
ax.legend(loc='upper left', frameon=False)
fig.tight_layout()
fig.savefig('../images/density_plot.png')



############### MIDAS

df = pd.read_pickle('../data/midas_results.pkl')

# first plot
fig, ax = plt.subplots(figsize=(10,3))

# ax.plot(returns)
# ax.set_ylim([-5, 5])

#ax2 = ax.twinx()
ax.plot(df['vola'], color='C2', label=r'$\sqrt{g \times \tau}$')
ax.axvline(cutoff, color='grey', ls=':', label='Train/test split', zorder=1)
ax.plot(df['sqrt(g)'], color='C0', label=r'$\sqrt{g}$ (short-run)')
ax.plot(df['sqrt(tau)'].resample('1M').last(), label=r'$\sqrt{\tau}$ (long-run)', c='C1')
#ax.axvline(cutoff, color='grey', ls=':', label='Train/test split')
ax.set_ylabel('Volatility')
ax.set_ylim([0, 5])
ax.set_yticks([0,1,2,3,4, 5])
ax.legend(loc='upper center', frameon=False, ncol=2)

fig.tight_layout()
fig.savefig('../images/entire_sample.png')



# second plot
VaRs = pd.read_pickle('../data/var_midas.pkl')
forecasts_midas = pd.read_pickle('../data/fct_midas.pkl')

fig = plt.figure(figsize=(10,3))
gs=GridSpec(1,2) 

ax1=fig.add_subplot(gs[0,0]) # First row, first column
ax2=fig.add_subplot(gs[0,1]) # First row, second column

ax1.plot(VaRs.iloc[:,0], label='Actual')
ax1.plot(VaRs.iloc[:,1],lw=.7, label='90%')
ax1.plot(VaRs.iloc[:,2],lw=.7, label='95%')
ax1.plot(VaRs.iloc[:,3],lw=.7, label='99%')
ax1.legend(frameon=False,ncol=4)
ax1.set_ylabel('Returns')
ax1.set_xlabel('')
ax1.xaxis.set_major_formatter(FmtY)
ax1.set_xticks(['2013', '2014', '2015'])
ax1.set_title('Value at Risk')

# already vola
forecasts_midas.plot(ax=ax2, color=['C1', 'C2'], rot=0,lw=.9, label='Predictions')
ax2.plot(np.sqrt(forecasts_nasdaq['true']), label='Actual - Realized kernel', zorder=1, alpha=.9)
ax2.legend(frameon=False, ncol=1)
ax2.set_ylabel('Volatility')
ax2.xaxis.set_major_formatter(FmtY)
ax2.set_xticks(['2013', '2014', '2015'])
ax2.set_title('Forecasted volatility')

fig.tight_layout()
fig.savefig('../images/appendix_midas.png')




























# x = np.linspace(-3, 3, 100)

# # def filter_garch(x, theta):

# #     h = np.empty(len(x))
# #     for t in range(len(x)-1):
# #         h[t+1] = theta[0] + theta[1]*x[t] + theta[2]*h[t]
# #     return h

# # filter_garch(x, [0, 0.05, 0.9])

# ni_garch = 0.2 * x**2

# term1 = 0.17 * x**2
# term2 = 0.05 * x[50:]**2
# term1[50:] = term1[50:] + term2


# ni_garch_gjr = term1
# plt.plot(x, ni_garch)
# plt.plot(x, ni_garch_gjr)



# def filter_gjr(x, theta):

# def filter_egarch(x, theta):
