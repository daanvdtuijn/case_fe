import pandas as pd
import numpy as np
import datetime as dt
import matplotlib.pyplot as plt
import math

class VolatilityMeasures:

    '''
    Class that contains multiple measures for realized variance/volatility of high frequency (per sec.) data:

        - rv: Realized variance (Slide 21)
        - bv: Bipower variance (Barndorff - Nielsen & Shephard (2004))
        - rk: Realized kernel (Barndorff - Nielsen et al. (2009))

    '''

    def __init__(self, stock: pd.Series, frequency = 5, take_average = False):
        
        self.stock = stock
        self.rv = None
        self.bv = None 
        self.rk = None
        
        ###
        self.frequency = frequency
        self.take_average_flag = take_average
        
    def measure(self, method = 'all'):
        
        ''' calls and stores calculations of desired volatility measures
        
            input:  stock
            
            params: which: {'rv', 'bv', 'rk', 'all'} default 'all'
        
        '''
        
        # stock is a timeseries consisting of multiple days 
        # where each day consists of transactions indexed by seconds
        # grouped by days
        self.daily_prices = self.stock.resample("1D")
        self.scaler = 100
        
        if method == 'all':
            # volatilies
            self.rv = self.calculate_rv()
            self.bv = self.calculate_bv()
            self.rk = self.calculate_rk()
        
        elif method == 'rv':
             self.rv = self.calculate_rv()
        elif method == 'bv':
             self.bv = self.calculate_bv()
        elif method == 'rk':
             self.rk = self.calculate_rk()
        elif method == 'tsrv':
             self.tsrv = self.calculate_tsrv()
        
    def subsampler(self, one_day_prices: pd.Series) -> pd.Series:
        
        ''' used for rv and bv'''
        
        # start & end record from whole day
        start  = one_day_prices.index[0]
        end    = one_day_prices.index[-1]
        # get a timestamp each 5 minutes
        freq_range = pd.date_range(start, end, freq=f'{self.frequency}min')
        # create empty series that is filled in with nearest observations 
        # at each 5 minute to create subsample
        subsample_empty = pd.Series(freq_range, name='timestamp')
        subsample = pd.merge_asof(subsample_empty, one_day_prices, 
                                  left_on='timestamp', right_index=True, 
                                  direction='nearest')
        return subsample.set_index('timestamp')
                                                     
    def estimator_rv(self, one_day_prices: pd.Series) -> float:
        
        ''' used for rv'''
        
        if (len(one_day_prices) > 0):
            # default is False
            if self.take_average_flag:
                # calculate sample by averaging
                subsample = one_day_prices.resample(f'{self.frequency}min').mean().fillna(method='ffill')
            else:
                # calculate sample by only keep values at 5 min - discard the rest
                subsample = self.subsampler(one_day_prices)
                
            # first calculate returns, then variance  
            ret = np.log(subsample).diff().dropna()
            var = np.sum( ret**2 )
            return var
        else:
            return np.nan
        
    def estimator_bv(self, one_day_prices: pd.Series) -> float:
        
        ''' used for bv'''
    
        if (len(one_day_prices) > 0):
            if self.take_average_flag:
                subsample = one_day_prices.resample(f'{self.frequency}min').mean().fillna(method='ffill')
            else:
                subsample = self.subsampler(one_day_prices)
            
            # first calculate absolute returns, then use bipower formula
            ret = np.abs( np.log(subsample).diff().dropna() )
            var = (np.pi/2.0) * np.sum( ret[1:]*ret[:-1] )
            return var
        else:
            return np.nan

    def estimator_tsrv(self, one_day_prices, K=300, J=1):

        if (len(one_day_prices) > 0):
            X = np.log(one_day_prices)
            n = len(X)
            nbarK = (n - K + 1) / K
            nbarJ = (n - J + 1) / J
            adj = (1 - (nbarK/nbarJ))**(-1) # correction factor

            selects = [np.arange(k, n, K) for k in np.arange(K)]
            retK = [X.take(select).diff().dropna().values for select in selects]
            retK = np.asarray([x for arr in retK for x in arr])

            selects = [np.arange(j, n, J) for j in np.arange(J)]
            retJ = [X.take(select).diff().dropna().values for select in selects]
            retJ = np.asarray([x for arr in retJ for x in arr])

            TSRV = adj * ((1/K) * np.sum(retK**2) - ((nbarK/nbarJ) *  (1/J) * np.sum(retK**2)))
            print(TSRV)
            return TSRV
        else:
            return np.nan
        
    def rv_sparse(self, one_day_prices: pd.Series, z = 1200) -> float:
        
        ''' used for rk to calculate bandwith'''
        
        # create new logarithmic timeseries from day prices with record at each second by forwardfilling nans
        ts_seconds = np.log( one_day_prices.resample('1S').pad() )
        # one hell of a one-liner: pg1 from their extra explanation
        z_rv_sparse = [np.sum(ts_seconds[i:].resample(f'{z}S').agg('first').diff()**2) for i in range(z)]
        return np.mean(z_rv_sparse)
    
    def omega(self, one_day_prices: pd.Series, q = 25) -> float:
        
        ''' used for rk to calculate bandwith'''

        # create q amount of time series: pg2 from extra explanation file 
        q_series = [np.log(one_day_prices.iloc[i::q]).diff() for i in range(q)]
        # formula for omegas: pg2
        q_omegas = [np.sum(series**2) / (2*len(series[series!=0]) ) for series in q_series]            
        return np.mean(q_omegas)
        
    
    def bandwidth(self, one_day_prices: pd.Series) -> float:
            
        ''' used for rk estimator '''
        
        # formula on .......
        n = len(one_day_prices) - 1 
        chi = np.sqrt( self.omega(one_day_prices) / self.rv_sparse(one_day_prices) ) 
        bandwidth = 3.5134 * chi**0.8 * n**0.6
        return math.ceil(bandwidth)
                
    def parzen_kernel(self, x: float) -> float:
            
        ''' used for rk estimator '''
        
        x = abs(x) 
        if (0 <= x <= 0.5):
            k = 1 - 6*x**2 + 6*x**3
        elif (0.5 <= x <= 1):
            k = 2*1-x**3
        else:
            k = 0
        return k
                
    def estimator_rk(self, one_day_prices: pd.Series) -> float:
        
        ''' used for rk'''
        
        if (len(one_day_prices) > 0):
            # calculate log returns from original day sample
            ret = np.log(one_day_prices).diff().dropna()
            # all below is from formula for the non-negative estimator on page C3 equation (1.2)
            n = len(ret)
            H = self.bandwidth(one_day_prices)
            RK = 0
            for h in np.arange(-H, H):
                parzen = self.parzen_kernel( h/(H+1) )  
                gamma = np.sum([ (ret[j]*ret[j-abs(h)]) for j in range(1+abs(h), n) ])
                RK += parzen*gamma
            
            print(RK)
            return RK
        else:
            return np.nan
        
    def calculate_rv(self) -> pd.Series:
        rv = self.daily_prices.apply(self.estimator_rv)
        return rv.rename("rv").dropna()

    def calculate_bv(self) -> pd.Series:
        bv = self.daily_prices.apply(self.estimator_bv)
        return bv.rename("bv").dropna()

    def calculate_tsrv(self):
        tsrv = self.daily_prices.apply(self.estimator_tsrv)
        return tsrv.rename("tsrv").dropna()

    def calculate_rk(self) -> pd.Series:
        rk = self.daily_prices.apply(self.estimator_rk)
        return rk.rename("rk").dropna()

    def plot_volatilities(self, return_fig = False) -> object:
        
        try:
            fig, ax = plt.subplots(figsize=(10,5))
            if self.rk is not None:
                np.sqrt(self.rk).plot(ax=ax, rot=0)
            if self.bv is not None:
                np.sqrt(self.bv).plot(ax=ax, rot=0)
            if self.rv is not None:
                np.sqrt(self.rv).plot(ax=ax, rot=0)
   
            ax.legend()
            ax.set_ylabel("volatility")
            
            if return_fig:
                return fig
        
        except:
            raise ValueError("Run calculations first on a stocks' time series ... Use .measure()!")   

