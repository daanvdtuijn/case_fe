# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 15:17:17 2021

@author: jensb


Use values of ML estimation in the paper as initial values in our dataset!

"""

import pandas as pd
import numpy as np
from scipy.optimize import minimize





DAL = pd.read_pickle('../../data/DAL_timeseries.pkl')
dfX = pd.read_csv('stock_returns.csv')
x = dfX['x'].values

ixic = pd.read_pickle('../../data/IXIC_volas.pkl').rk_parzen[:len(x)]

