import numpy as np
import pandas as pd
import scipy
from numpy.linalg import inv, norm
from scipy import stats

################################  OPTIM CODE NOT WORKING NOW
# MV Newton-Raphson
# def newton(func, x0, param, error, maxit):
#     # Input: a function (func), an initial value (x0),
#     #        the parameters of the function (param),
#     #        the error set for the optim. (if Xt-Xt-1 =< error -> stop),
#     #        the maximum number of iterations (maxit)
#     # Output: Max value of the function.
#     #
#     #
#     h = np.diag(x0*1.e-4)
#     n = len(x0)
#     J = np.empty((n, n))
#
#     for i in range(0, maxit):
#         f = func(x0, param)
#         for j in range(n):
#             J[:, j] = (f-func(x0-h[:, j], param))/h[j, j]
#         x = x0 - np.matmul(inv(J), f)
#         if norm(x-x0) < error:
#             break
#         x0 = x
#     if i >= maxit:
#         print('Reached max number of iterations')
#     return x

# # Univariate Newton-Raphson
# def newton1(func, param, x0, error, maxit):
#     h = x0*1.e-4
#     for i in range(0, maxit):
#         f = func(param, x0)
#         df = (f-func(param, x0-h))/h
#         x = x0 - f/df
#         if norm(x-x0) < error:
#             break
#         x0 = x
#     if i >= maxit:
#         print('Reached max number of iterations')
#     return x


################################  Volatility model's class
class volatility_models():
    '''
    Class that contains the several volatility-estimation models:
        * GARCH
        * E-GARCH
    '''

    def __init__(self, theta, x, model):
        '''
        :param theta: initial parameters
        :param x:     stock returns
        :param model: volatility models (eg. "GARCH", "EGARCH")
        '''

        self.theta    = theta
        self.x        = x

        # models
        self.model = eval("self.calculate_"+ model+"()")

    def ll_GARCH(self, theta, x):
        # Log-likelihood GARCH function
        T = len(x)
        omega = theta[0]
        alpha = theta[1]
        beta = theta[2]

        # Filter Volatility
        sig = np.zeros(T)
        sig[0] = np.var(self.x)  # initialize volatility at unconditional variance

        for t in range(0, T - 1):
            sig[t + 1] = omega + alpha * self.x[t] ** 2 + beta * sig[t]

        ## Calculate Log Likelihood Values

        # construct sequence of log lik contributions
        l = -(1 / 2) * np.log(2 * np.pi) - (1 / 2) * np.log(sig) - (1 / 2) * (self.x ** 2) / sig
        # squared here is a function defined above
        # mean log likelihood
        return float(-np.mean(l))

    def ll_e_GARCH(self, theta, x):

        pass

    def calculate_GARCH(self):
        # optimizing GARCH function
        results = scipy.optimize.minimize(self.ll_GARCH, self.theta, args= self.x,
                                          # options=options,
                                          method='SLSQP', bounds=((0.00001, 100),(0, 10),(0, 1)))
        return results

    def print_results(self):
        print('parameter estimates:')
        print(self.model.x)

        print('log likelihood value:')
        print(self.model.fun)

        print('exit flag:')
        print(self.model.success)
################################

### EXAMPLE WITH GARCH MODEL
## Inputs:
# Stock returns
DAL = pd.read_pickle('../data/DAL_timeseries.pkl')
# Initial Parameters
omega_ini = 0.1  # initial value for omega
alpha_ini = 0.2  # initial value for alpha
beta_ini = 0.8   # initial value for beta
theta_ini = np.array([omega_ini,
                      alpha_ini,
                      beta_ini])
# Options por optim
options ={'eps':1e-09,
         'disp': True,
         'maxiter':200}

# GARCH:
garch = volatility_models(theta= theta_ini, x= DAL, model ="GARCH")
garch.print_results()