import numpy as np
import pandas as pd

class utils:
    # def __init__(self):
    #     pass
    

    def import_daily_return(self, path):
        # calculate daily returns
        data = pd.read_pickle(path)
        price_daily = data.resample('1D').last().ffill()
        ret_daily = np.log(price_daily) - np.log(price_daily.shift())
        return np.array(ret_daily.dropna())