import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy
from scipy.optimize import fmin_slsqp
from scipy import stats
from numpy.linalg import inv, norm
from utils import utils
from scipy import optimize
from scipy.special import gamma, gammaln
#from scipy.stats import gamma
import warnings
warnings.filterwarnings("ignore")


class GAS():
    '''

    GAS

    '''

    # defualt attributes

    method = 'SLSQP'

    # unit scaling: scaling matrix S is set to 1
    S = 1
    scaler = 100
    options = {'eps': 1e-09,
            'disp': True,
            'maxiter': 100}


    def __init__(self, x, theta, dist):
        '''
        data: daily return
        ar: ar lags
        sc: sc lags
        dist: distribution
        '''
        
        self.x = x * self.scaler
        self.theta = theta
        self.dist = dist
        

        if self.dist == 'normal':
            self.bounds = ((0.00001, 1),
                        (0, 10),
                        (0, 10))

        if self.dist == 't':
            self.bounds = ((0.00001, 1),
                        (0, 10),
                        (0, 10),
                        (1, 50))
        
        if self.dist == 'GED':
            self.bounds = ((0.00001, 1),
                        (0, 10),
                        (0, 10),
                        (0, 5))

    def filter_GAS(self, theta, x):
        '''
        Return filtered f

        theta: initial parameters
        x: data, daily returns
        '''
        
        T = len(x)
        
        # number of parameters 
        self.k = len(theta)

        # parse initial parameter values
        if self.dist == 'normal':
                
            # intercept
            omega = theta[0]
            # assuming p and q in score updating are both 1, A and B are numbers
            A = theta[1]
            B = theta[2]

        if self.dist == 't':
            
            # intercept
            omega = theta[0]
            # assuming p and q in score updating are both 1, A and B are numbers
            A = theta[1]
            B = theta[2]

            # degree of freedom for student t distribution
            self.v = theta[3] # make v global

        if self.dist == 'GED':
            
            # intercept
            omega = theta[0]
            # assuming p and q in score updating are both 1, A and B are numbers
            A = theta[1]
            B = theta[2]
            # v
            self.v = theta[3] # make v global


        # x is demeaned
        mu = x.mean()
        self.eps = x - mu

        # initialize f and s arrays
        f = np.zeros(T)
        s = np.zeros(T)
        l = np.zeros(T)
        score = np.zeros(T)
        score_ALT = np.zeros(T)
        S = np.zeros(T)

        # set initial values
        var = np.var(self.x)
        # f in this case is the variance of x

        if self.dist == 'normal': f[0] = np.log(var)
        if self.dist == 't': f[0] = var
        if self.dist == 'GED': f[0] = np.log(var)


        for t in range(0, T-1):
            
            if self.dist == 'normal':

                # l[t] = - 0.5*(np.log(2*np.pi) + np.log(f[t]) + eps[t]**2/f[t])
                #l[t] = -(1/2)*np.log(2*np.pi) - (1/2) * np.log(f[t]) - (self.eps[t]**2/(2*f[t]))

                # calculate the score of the log likelihood function with respect to f

                if f[0] == np.log(var):
                    # for S == 1
                    score[t] = (1 / (2 * np.exp(f[t])**2) * (self.eps[t]**2 - np.exp(f[t])))  * np.exp(f[t])
                    S[t] = self.S
                
                if f[0] == var:
                    # for S != 1
                    score[t] = (1 / (2 * f[t]**2) * (self.eps[t]**2 - f[t])) # * f[t]
                    S[t] = 2*f[t]**2

                else:
                    print('Initial f value is not equal to variance or log-variance.')


            if self.dist == 't':

                # calculate the score of the negative log likelihood function with respect to f

                w = (self.v + 1)/((self.v - 2) + (self.eps[t] ** 2/ f[t]))

                # for S != 1
                score[t] = (1 / (2 * f[t] ** 2)) * ((w * self.eps[t]**2)/f[t] - 1)

                # calcualte S, the scaling matrix
                S[t] = (2* (self.v +3)*f[t]**2)/self.v
                #S[t] = self.S
            

            if self.dist == 'GED':
                
                if f[0] == np.log(var):

                    # unit scaling
                    lmb = (gamma((1/self.v)) / ((2 ** (2/self.v)) * gamma(3/self.v))) ** (1/2)

                    score[t] = (-(1/2*np.exp(f[t])) - (1/2) * self.v * ((self.eps[t] / (abs(lmb) * abs(np.exp(f[t]) ** 0.5))) ** (self.v - 1)) \
                                * (self.eps[t] / abs(lmb)) * ((- (1 / abs(np.exp(f[t]) ** 0.5) ** 2)) * (1 / (2 * abs(np.exp(f[t]) ** 0.5))))) * np.exp(f[t])
                    
                    # unit scaling since it is difficult present an analytical formulation of S
                    S[t] = self.S
                    print(f'score {score[t]}')


            # calculate the scaled score at time t
            s[t] = S[t] * score[t]
            
            # updating equation: calculate f at time t + 1
            f[t+1] = omega + A * s[t] + B * f[t]
            
        return f
    

    def _calc_score(self, x):
        '''
        numerically estimate the score of the llk function
        '''

        def func(x, c0, c1):
            "Coordinate vector `x` should be an array of size two."
            return c0 * x[0]**2 + c1*x[1]**2

        c0, c1 = (1, 200)
        eps = np.sqrt(np.finfo(float).eps)
        
        return optimize.approx_fprime(x, func, [eps, np.sqrt(200) * eps], c0, c1)[0]

    def _calc_llk(self, f, dist = None):
        '''
        calculate the log likelihood of a specified distribution, which include normal, student t, and generalized error distribution
        '''

        if dist == None: dist = self.dist
        

        if dist == 'normal':

            #l = -(1 / 2) * np.log(2 * np.pi * f) - (eps / (2 * f))
            #l = 0.5*(np.log(2*np.pi) + np.log(sigma2) + eps**2/sigma2)
            
            l = -(1/2)*np.log(2*np.pi) - (1/2) * np.log(f) - (self.eps**2/(2*f))

        if dist == 't':

            # l = np.log(gamma((self.v+1)/2)) - np.log(gamma(self.v/2)) - 0.5*np.log((self.v - 2) * np.pi * f) \
            #     - ((self.v + 1) / 2) * np.log(1 + (self.eps ** 2)/((self.v - 2) * f))
            l = np.log(stats.t.pdf(self.eps/(f**0.5), df = self.v)) - (1/2)*np.log(f)


        if dist == 'GED':

            lmb = (gamma(1/self.v) / ((2 ** (2/self.v)) * gamma(3/self.v))) ** (1/2)

            l = - np.log(2 ** (1 + (1/self.v)) * gamma(1/self.v) * lmb) - 0.5 * np.log(f) + np.log(self.v) \
                    - 0.5 * abs((self.eps) / (lmb * (f ** 0.5))) ** self.v

        
        return l

    def llik_GAS(self, theta, x, out = None):
        '''
        log-likelihood function of GAS model
        theta -> List: parameter vector
        x -> np.array/pd.Series: daily returns 
        '''


        # Log-likelihood GAS function

        f = self.filter_GAS(theta, x)

        # construct sequence of log lik contributions
 
        l = self._calc_llk(f)


        if out == True:
            return -np.mean(l), -l
        else:
            return -np.mean(l)  


    def fit(self):
        '''
        fit the GAS model
        '''


        results = scipy.optimize.minimize(self.llik_GAS, self.theta, args=(self.x),
                                          options=self.options,
                                          method=self.method,
                                          bounds = self.bounds)

        return results



    

if __name__ == "__main__":

    # calculate daily returns
    utils = utils()
    path = '../../data/DAL_timeseries.pkl'

    # x: daily return
    x = utils.import_daily_return(path)

    dist = 'GED'
    theta_ini = utils.get_initial_parameters(dist = dist)

    model = GAS(theta=theta_ini, x=x, dist = dist)
    print(model.fit())