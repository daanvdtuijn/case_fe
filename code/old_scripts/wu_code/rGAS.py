import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy
from scipy.optimize import fmin_slsqp
from scipy import stats
from numpy.linalg import inv, norm
from utils import utils
from scipy import optimize


class rGAS():
    '''

    rGAS

    '''

    # defualt attributes

    method = 'SLSQP'
    # scaling matrix S is set to 1 for the time being
    S = 1
    scaler = 100
    options = {'eps': 1e-09,
            'disp': True,
            'maxiter': 100}

    bounds = ((0.00001, 1),
                (0, 10),
                (0, 10))

    def __init__(self, x, theta, realized_variance):
        '''
        data: daily return
        ar: ar lags
        sc: sc lags
        dist: distribution
        '''
        
        self.x = x * self.scaler
        self.theta = theta
        self.realized_variance = realized_variance

        # self.ar = ar
        # self.ma = sc
        # self.dist = dist

    def filter_GAS(self, theta, x):
        '''
        Return filtered f
        '''
        
        T = len(x)
        
        # number of parameters 
        self.k = len(theta)

        # intercept
        omega = theta[0]
        # assuming p and q in score updating are both 1, A and B are numbers
        A = theta[1]
        B = theta[2]


        log_h = np.log(np.var(x))
        rv = np.array(self.realized_variance)
        log_x = np.log(rv)
        # x is demeaned
        mu = x.mean()
        self.eps = x - mu



        # initialize f and s arrays
        f = np.zeros(T)
        s = np.zeros(T)
        l = np.zeros(T)
        score = np.zeros(T)
        score_ALT = np.zeros(T)
        S = np.zeros(T)

        # set initial values
        var = np.var(x)
        # f in this case is the variance of x
        f[0] = var


        for t in range(0, T-1):
            
            # l[t] = - 0.5*(np.log(2*np.pi) + np.log(f[t]) + eps[t]**2/f[t])
            l[t] = -(1/2)*np.log(2*np.pi) - (1/2) * np.log(f[t]) - (self.eps[t]**2/(2*f[t]))

            # calculate the score of the negative log likelihood function with respect to f
            score[t] = (1 / (2 * f[t]**2) * (self.eps[t]**2 - f[t]))
            score_ALT[t] = self._calc_score2(l[t:t+2])

            print(f'score {score[t]}')
            print(f'score_ALT {score_ALT[t]}')


            # calcualte S, the scaling matrix
            S[t] = 2*f[t]**2
            #S[t] = self.S


            # calculate the scaled s at time t
            s[t] = S[t] * score[t]

            # calculate f at time t + 1
            f[t+1] = omega + A * s[t] + B * f[t]

            
        return f
    

    def _calc_score(self, f_t, llk_t):
        '''
        f_t -> float:  f at time t
        llk_t -> float: loglikelihood at time t
        '''

        # h is a small number based on f
        h = 1e-5 * f_t
        delta = h
        llk_plus, llk_minus = (llk_t + delta), (llk_t-delta)

        return (llk_plus - llk_minus) / (2 * h)


    def _calc_score2(self, x):

        def func(x, c0, c1):
            "Coordinate vector `x` should be an array of size two."
            return c0 * x[0]**2 + c1*x[1]**2

        c0, c1 = (1, 200)
        eps = np.sqrt(np.finfo(float).eps)
        
        return optimize.approx_fprime(x, func, [eps, np.sqrt(200) * eps], c0, c1)[0]



    def llik_GAS(self, theta, x, out = None):
        '''
        log-likelihood function of GAS model
        theta -> List: parameter vector
        x -> np.array/pd.Series: daily returns 
        '''


        # Log-likelihood GAS function

        f = self.filter_GAS(theta, x)

        # construct sequence of log lik contributions
        #l = -(1 / 2) * np.log(2 * np.pi * f) - (eps / (2 * f))


        #l = 0.5*(np.log(2*np.pi) + np.log(sigma2) + eps**2/sigma2)

        l = -(1/2)*np.log(2*np.pi) - (1/2) * np.log(f) - (self.eps**2/(2*f))


        # np.mean(l)
        # squared here is a function defined above
        # mean log likelihood

        if out == True:
            return -np.mean(l), -l
        else:
            return -np.mean(l)  


    def fit(self):





        results = scipy.optimize.minimize(self.llik_GAS, self.theta, args=(self.x),
                                          options=self.options,
                                          method=self.method,
                                          bounds = self.bounds)

        return results

    

    # def print_results(self):
    #     print('parameter estimates:')
    #     print(self.calculate_GARCH().x)

    #     print('log likelihood value:')
    #     print(self.calculate_GARCH().fun)

    #     print('exit flag:')
    #     print(self.calculate_GARCH().success)


if __name__ == "__main__":

    # calculate daily returns
    utils = utils()
    path = '../../data/DAL_timeseries.pkl'
    ret_daily = utils.import_daily_return(path)
    x = ret_daily


    # dfX = pd.read_csv('../pedro_garch(s)/stock_returns.csv')
    # x = dfX['x'].values



    omega_ini = 0.1  # initial value for omega
    A_ini = 0.05  # initial value for alpha
    B_ini = 0.85  # initial value for beta

    theta_ini = np.array([omega_ini,
                        A_ini,
                        B_ini])
    # options por optim



    stock = GAS_models(theta=theta_ini, x=x)
    #print(stock.llk_gas(theta_ini, x))
    print(stock.fit())