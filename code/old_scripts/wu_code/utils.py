import numpy as np
import pandas as pd

class utils:
    # def __init__(self):
    #     pass
    

    def import_daily_return(self, path):
        # calculate daily returns
        data = pd.read_pickle(path)
        price_daily = data.resample('1D').last().ffill()
        ret_daily = np.log(price_daily) - np.log(price_daily.shift())
        return np.array(ret_daily.dropna())


    def get_initial_parameters(self, dist):

        '''
        get initial parameters for implemented distributions (normal, t, GED)
        '''

        omega_ini = 0.1  # initial value for omega
        A_ini = 0.05  # initial value for A
        B_ini = 0.95  # initial value for B
        v_ini = 5
        

        if dist == 'normal':
            
            theta_ini = np.array([omega_ini,
                                A_ini,
                                B_ini])
        if dist == 't':
                    
            theta_ini = np.array([omega_ini,
                                A_ini,
                                B_ini,
                                v_ini])

        if dist == 'GED':

            omega_ini = 0.01  # initial value for omega
            A_ini = 0.05  # initial value for A
            B_ini = 0.90  # initial value for B
            v_ini = 0.5
            theta_ini = np.array([omega_ini,
                                A_ini,
                                B_ini,
                                v_ini])

        return theta_ini