import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.optimize import fmin_slsqp

# ### AA
# stock = pd.read_pickle('../data/AA_timeseries.pkl')
# # c2c
# closing_prices = stock.resample('1D').agg('last').dropna()

### NASDAQ
df = pd.read_pickle('../data/IXIC_volas.pkl')
df.index = pd.to_datetime(df.index)
closing_prices = df['close_price']

# calulcate returns
ret = (np.log(closing_prices)*100).diff().dropna()
ret = ret - ret.mean()

# ### AA
# var_measures = pd.read_pickle('../data/volatility_measures.pkl')
# rk = np.abs(var_measures['rk']) * 2 * 100**2 # correction factor

## NASDAQ
rk = df['rk_parzen']* 2 * 100**2
# take quarters
rk = rk.resample('1M').sum().resample('B').fillna('bfill')[1:]

# make sure returns are aligned with rk
ret = ret.loc[rk.index[0]:] 
ret = ret - ret.mean()
# and (daily index) montlhy realized variances aligned with ret
rk = rk[ret.index]

theta_names = ['alpha', 'beta', 'm', 'zeta', 'w']
theta_ini   = [0.01, 0.85, 0.1, 0.001, 1]


def beta_weight_scheme(k, K, w1, w2=1):
    num = ((k/K)**(w1-1)) * (((1-k)/K)**(w2-1))
    denom = np.sum( [((j/K)**(w1-1)) * (((1-j)/K)**(w2-1)) for j in range(1, K+1)] )
    return num/denom

def filtering(theta, ret=ret, X=rk):

    ''' 
    theta -> params to be optimized
    ret   -> returns daily
    X     -> exogenous variable monthly with daily index
    '''

    # unpack params
    alpha, beta, m, zeta, w = theta
    # lags in quarters
    K = 6
    # make monthly index
    X_low_freq = X.resample('1M').last()
    # calculate weights for lags using beta weight function, take sum each loop to calculate tau
    tau = np.empty(X_low_freq.size)
    for t in range(X_low_freq.size):
        # lagged exogenous variables with appropriate weight coefficient
        terms = [beta_weight_scheme(k, K, w)*X_low_freq[t-k] if (t-k) >= 0 else 0 for k in range(1,K+1)] 
        tau[t] =  m + zeta*np.sum(terms)   
    tau = pd.Series(tau, name='tau', index=X_low_freq.index)
    df = pd.merge_asof(ret, tau, left_index=True, right_index=True, direction='forward')
    tau = df['tau']
    g = pd.Series(name='g', index=ret.index)
    g[0] = np.var(ret)
    # the model update equation
    for t in range(ret.size-1):
            g[t+1] = (1-alpha-beta) + alpha*((ret[t]**2)/tau[t]) + beta*g[t]
    return g, tau

def log_lik(theta, ret=ret, X=rk, output='optim'):

    g, tau = filtering(theta)
    #if self.dist == 'Gaussian':
    l = -0.5*( np.log(2*np.pi) + np.log(g*tau) + (ret**2)/(g*tau) )
    
    if output == 'optim':
        return -np.mean(l)
    elif output == 'hessian':
        return -np.sum(l)
    elif output == 'SE':
        return l

def fit(mthd='SLSQP', bnds=((0, 1), (0, 1), (0, 10), (0, 1), (0, 30)) ):

    def constraint(theta): 
        return np.array(1-theta[0]-theta[1]) # alpha + beta < 1
    constraint={"fun": constraint, 'type': 'ineq'}
    # fit/optimize
    result = minimize(log_lik, theta_ini, method=mthd,
                      constraints=constraint, bounds=bnds, options={'disp': True})
    return result

g_ini, tau_ini = filtering(theta_ini)

res = fit()
g, tau = filtering(res.x)

print(res)
print(theta_ini)

fig,ax=plt.subplots(figsize=(15,4))
ax.plot(g*tau, label='midas')
ax.plot(g, label='g: long-run')
ax.plot(tau, label='tau: long-run')
ax.legend()
ax.plot(model.variance, label='normal garch', color='grey', alpha=.5)
ax.legend()

fig,ax=plt.subplots()
ax.plot(tau)
ax.plot(tau_ini, label='initial')
ax.legend()





from volatility_models import *
model = GARCH(closing_prices)
model.fit()

model.variance.plot()
