
## ECONOMETRIC METHODS FOR FORECASTING
#
#  MAXIMUM LIKELIHOOD ESTIMATE PARAMETERS OF GARCH
#
#  Description:
#  This code snippet shows how to optimize the log likelihood
#  and estimate the parameters  of a GARCH model by maximum likelihood.
#
#  Francisco Blasques 2019


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mat4py import loadmat
import scipy
from scipy import stats
from llik_fun_GARCH import llik_fun_GARCH

## 1. Setup
dfX  = pd.read_csv('stock_returns.csv')

#x = loadmat('stock_returns.mat')
#dfX =  pd.DataFrame({'x': np.array(x['x'][:])[:,0]})

x = dfX['x'].values


## 2. Optimization Options

options ={'eps':1e-09,
         'disp': True,
         'maxiter':200}

## 4. Initial Parameter Values

omega_ini = 0.1  # initial value for omega
alpha_ini = 0.2  # initial value for alpha
beta_ini = 0.8   # initial value for beta

theta_ini = np.array([omega_ini,
                      alpha_ini,
                      beta_ini

                     ])




## 5. Optimize Log Likelihood Criterion

#  optim input:
# (1) negative log likelihood function: llik_fun_GARCH() note the minus when calculating the mean
# (2) initial parameter: theta_ini
# (3) parameter space bounds: lb & ub
# (4) optimization setup: control
#  Note: a number of parameter restriction are left empty with []

#  optim output:
# (1) parameter estimates: par
# (2) negative mean log likelihood value at theta_hat: value
# (3) exit flag indicating (no) convergence: convergence


results = scipy.optimize.minimize(llik_fun_GARCH, theta_ini, args=(x),
                                  options = options,
                                  method='SLSQP', bounds=( (0.00001,  100), # Parameter Space Bounds
                                                              (0, 10),
                                                              (0, 1)
                                                            )) #restrictions in parameter space




## 7. Print Output

print('parameter estimates:')
print(results.x)

print('log likelihood value:')
print(results.fun)

print('exit flag:')
print(results.success)
#results$success #if exitflag=0: convergence ok
         #if exitflag>0: convergence failed
         # see also results$message for additional information returned by the optimizer
