import numpy as np


def llik_fun_GARCH(theta,x):
    T=len(x);
    omega=theta[0]
    alpha=theta[1]
    beta=theta[2]

    # Filter Volatility
    sig = np.zeros(T)
    sig[0] = np.var(x); #initialize volatility at unconditional variance

    for t in range(0,T-1):
        sig[t+1] = omega + alpha*x[t]**2 + beta*sig[t]

    ## Calculate Log Likelihood Values

    #construct sequence of log lik contributions
    l = -(1/2)*np.log(2*np.pi) - (1/2)*np.log(sig) - (1/2)*(x**2)/sig #squared here is a function defined above

    # mean log likelihood
    return -np.mean(l)

