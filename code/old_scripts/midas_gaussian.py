import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.optimize import fmin_slsqp

# ### AA
# stock = pd.read_pickle('../data/AA_timeseries.pkl')
# # c2c
# closing_prices = stock.resample('1D').agg('last').dropna()

### NASDAQ
df = pd.read_pickle('../data/IXIC_volas.pkl')
df.index = pd.to_datetime(df.index)
closing_prices = df['close_price']

# calulcate returns
ret = (np.log(closing_prices)*100).diff().dropna()
ret = ret - ret.mean()

# ### AA
# var_measures = pd.read_pickle('../data/volatility_measures.pkl')
# rk = np.abs(var_measures['rk']) * 2 * 100**2 # correction factor

## NASDAQ
rk = df['rk_parzen']* 2 * 100**2
# take quarters
rk = rk.resample('1M').sum().resample('B').fillna('bfill')[1:]

# make sure returns are aligned with rk
ret = ret.loc[rk.index[0]:] 
ret = ret - ret.mean()
# and (daily index) montlhy realized variances aligned with ret
rk = rk[ret.index]

theta_names = ['alpha', 'beta', 'm', 'zeta', 'w']
theta_ini   = [0.01, 0.85, 0.1, 0.001, 1]

# ##########################
# import yfinance as yf
# import quandl

# # Producer Price Index by Commodity for Metals and Metal Products: Aluminum Sheet and Strip
#rk = quandl.get("FRED/WPU10250105", start_date="2007-03-01", end_date="2014-12-31")['Value'].copy()
# # unemployment rate
# rk = pd.DataFrame(quandl.get("FRED/UNRATE", start_date="2007-03-01", end_date="2014-12-31"))

# # Euro/Dollar
# rk = yf.download(tickers="EURUSD=X",  start="2007-03-01", end="2014-12-31")['Close']
# # VIX
# rk = yf.download(tickers="^VIX",  start="2007-03-01", end="2014-12-31")['Close']
##########################

def beta_weight_scheme(k, K, w1, w2=1):
    num = ((k/K)**(w1-1)) * (((1-k)/K)**(w2-1))
    denom = np.sum( [((j/K)**(w1-1)) * (((1-j)/K)**(w2-1)) for j in range(1, K+1)] )
    return num/denom

def filtering(theta, ret=ret, X=rk):

    ''' 
    theta -> params to be optimized
    ret   -> returns daily
    X     -> exogenous variable monthly with daily index
    '''

    # unpack params
    alpha, beta, m, zeta, w = theta
    # lags in quarters
    K = 6
    # make monthly index
    X_low_freq = X.resample('1M').last()
    # calculate weights for lags using beta weight function, take sum each loop to calculate tau
    tau = np.empty(X_low_freq.size)
    weights = [beta_weight_scheme(k, K, w) for k in range(1, K+1)]
    for t in range(X_low_freq.size):
        # lagged exogenous variables with appropriate weight coefficient
        terms = [weight*X_low_freq[t-k] if (t-k) >= 0 else 0 for k, weight in zip(range(1,K+1), weights)] 
        tau[t] =  m + zeta*np.sum(terms)   
    tau = pd.Series(tau, name='tau', index=X_low_freq.index)
    df = pd.merge_asof(ret, tau, left_index=True, right_index=True, direction='forward')
    tau = df['tau']
    g = pd.Series(name='g', index=ret.index)
    g[0] = np.var(ret)
    # the model update equation
    for t in range(ret.size-1):
            g[t+1] = (1-alpha-beta) + alpha*((ret[t]**2)/tau[t]) + beta*g[t]
    return g, tau

def log_lik(theta, ret=ret, X=rk, output='optim'):

    g, tau = filtering(theta)
    #if self.dist == 'Gaussian':
    l = -0.5*( np.log(2*np.pi) + np.log(g*tau) + (ret**2)/(g*tau) )
    
    if output == 'optim':
        return -np.mean(l)
    elif output == 'hessian':
        return -np.sum(l)
    elif output == 'SE':
        return l

def fit(mthd='SLSQP', bnds=((0, 1), (0, 1), (0, 10), (0, 1), (0, 30)) ):

    def constraint(theta): 
        return np.array(1-theta[0]-theta[1]) # alpha + beta < 1
    constraint={"fun": constraint, 'type': 'ineq'}
    # fit/optimize
    result = minimize(log_lik, theta_ini, method=mthd,
                      constraints=constraint, bounds=bnds, options={'disp': True})
    return result

g_ini, tau_ini = filtering(theta_ini)

res = fit()
g, tau = filtering(res.x)

print(res)
print(theta_ini)

fig,ax=plt.subplots(figsize=(10,3))
ax.plot(np.sqrt(g*tau), label='midas')
ax.plot(np.sqrt(g), label='g: short-run')
ax.plot(np.sqrt(tau.resample('1M').last()), label='tau: long-run')
ax.legend()
ax.plot(model.variance, label='normal garch', color='pink', alpha=.9)
ax.legend()


df = pd.DataFrame({'vola': np.sqrt(g*tau),
              'sqrt(g)': np.sqrt(g)})

df['sqrt(tau)'] = np.sqrt(tau)

df.to_pickle('../data/midas_results.pkl')



fig,ax=plt.subplots()
ax.plot(tau)
ax.plot(tau_ini, label='initial')
ax.legend()





from volatility_models import *
model = GARCH(closing_prices)
model.fit()

model.variance.plot()


class midasGARCH(VolatilityModel):

    '''
    Model: MIDAS_GARCH
    '''
    def __init__(self, stock, exog, low_freq='1M', split_date=None, dist='Gaussian'):
        super().__init__(stock, split_date, dist)

        self.theta_hat = None
        self.low_freq = low_freq
        # the exogenous variable
        self.exog = exog
        # for train/test
        self.exog_train = self.exog[:self.split_date] # scale before inserting
        self.exog_test  = self.exog[self.split_date:] # no scaling required

        if self.dist == 'Gaussian':
            self.theta_names =  ['alpha', 'beta', 'm', 'zeta', 'w', 'vega']
            self.theta_ini = [0.01, 0.85, 0.01, 0.01, 10, 50]
            self.bnds = ((0, 10), (0, 10), (0, 10), (0, 10), (0, 30), (0,100))

    def _beta_weight_scheme(self, k, K, w1, w2=1):
        num = ((k/K)**(w1-1)) * (((1-k)/K)**(w2-1))
        denom = np.sum( [((j/K)**(w1-1)) * (((1-j)/K)**(w2-1)) for j in range(1, K+1)] )
        return num/denom

    def filtering(self, theta, ret=None, X=None):

        ''' 
        theta -> params to be optimized
        ret   -> returns daily
        X     -> exogenous variable monthly with daily index
        '''
        if ret == None and X == None:
            ret = self.x_train
            X = self.exog_train

        # unpack params
        alpha, beta, m, zeta, w, _ = theta
        # lags in quarters
        K = 6
        # make weeekly, monthly, quarterly, .. etc. index
        X_low_freq = X.resample(self.low_freq).last()
        # calculate weights for lags using beta weight function, take sum each loop to calculate tau
        tau = np.empty(X_low_freq.size)
        weights = [self._beta_weight_scheme(k, K, w) for k in range(1, K+1)]
        for t in range(X_low_freq.size):
            # lagged exogenous variables with appropriate weight coefficient
            terms = [weight*X_low_freq[t-k] if (t-k) >= 0 else 0 for k, weight in zip(range(1,K+1), weights)] 
            tau[t] =  m + zeta*np.sum(terms)   
        tau = pd.Series(tau, name='tau', index=X_low_freq.index)
        df = pd.merge_asof(ret, tau, left_index=True, right_index=True, direction='forward')
        tau = df['tau']
        g = pd.Series(name='g', index=ret.index)
        g[0] = np.var(ret)
        # the model update equation
        for t in range(ret.size-1):
                g[t+1] = (1-alpha-beta) + alpha*((ret[t]**2)/tau[t]) + beta*g[t]

        if self.theta_hat == None:
            return g, tau
        else:
            return g*tau

    def log_likelihood(self, theta, ret=None, X=None, output='optim'):

        if ret == None and X == None:
            ret = self.x_train
            X = self.exog_train

        g, tau = self.filtering(theta)
        
        if self.dist == 'Gaussian':
            l = -0.5*( np.log(2*np.pi) + np.log(g*tau) + (ret**2)/(g*tau) )
        # if self.dist == 'Student':
        # v = theta[-1]
        # A = np.log(gamma((v+1)/2)) - np.log(np.sqrt(v*np.pi)) - np.log(gamma(v/2))
        # l = A - ((v + 1)/2) * np.log(1 + ((ret**2/g*tau) / v)) - np.log(np.sqrt(g*tau))
        
        if output == 'optim':
            return -np.mean(l)
        elif output == 'hessian':
            return -np.sum(l)
        elif output == 'SE':
            return l

    def fit(self, mthd='SLSQP'):

        def constraint(theta): 
            return np.array(1-theta[0]-theta[1]) # alpha + beta < 1
        constraint={"fun": constraint, 'type': 'ineq'}
        # fit/optimize
        self.result_optimization = minimize(self.log_likelihood, self.theta_ini, method=mthd,
                                            constraints=constraint, bounds=self.bnds)
        if self.result_optimization.success == True:
            self.set_estimation_values()
            self.show_estimation_values()
        else:
            print('Optimization failed...')

    def forecast(self):
        raise NotImplementedError

test_model = midasGARCH(closing_prices, rk)
test_model.fit()




