################################  GARCH CLASS MODEL
# pedro.i
# 12/01/21


import numpy as np
import pandas as pd
import scipy
from scipy.optimize import minimize

class GARCH():
    '''
    Class that contains the GARCH model
    '''

    def __init__(self, theta, x, dist):
        '''
        :param theta: initial parameters
        :param x:     stock returns
        :param dist:  error distrib
        '''

        self.theta    = theta
        self.x        = x
        # distribution
        self.distribution = dist

    def ll_GARCH(self, theta, x):
        """
        LL and sigma function

        """
        # Log-likelihood GARCH function
        T = len(x)
        omega = theta[0]
        alpha = theta[1]
        beta = theta[2]

        # Filter Volatility
        sig = np.zeros(T)
        sig[0] = np.var(self.x)  # initialize volatility at unconditional variance

        for t in range(0, T - 1):
            sig[t + 1] = omega + alpha * self.x[t] ** 2 + beta * sig[t]

        ## Calculate Log Likelihood Values for normal distribution
        if self.distribution == "normal":
            l = -(1 / 2) * np.log(2 * np.pi) - (1 / 2) * np.log(sig) - (1 / 2) * (self.x ** 2) / sig
        elif self.distribution == "student":
            pass
        ## Storing the volatility
        self.sigma = sig
        return float(-np.mean(l))


    def calculate_GARCH(self):
        """
        Optimizing LL function, storing results

        """
        # GARCH stationarity condition

        def stat_cond(theta):
            alpha = theta[1]
            beta  = theta[2]
            stat_cond = 1 - alpha - beta
            return stat_cond

        # Options for optimization
        options ={'eps':1e-09,
                  'disp': False,
                  'maxiter':200}

        # optimizing GARCH function
        results = scipy.optimize.minimize(self.ll_GARCH, self.theta, args= self.x,
                                          options=options,
                                          method='SLSQP', bounds=((0.00001, 100),(0, 10),(0, 1)),
                                          constraints = {"fun": stat_cond, 'type': 'ineq'})

        # storing results
        self.param = results.x
        self.logl = results.fun
        self.AIC = -2*self.logl+2*len(self.param)
        self.resid = (self.x / self.sigma)
        return results


################################

### EXAMPLE WITH GARCH MODEL
## Inputs:
# Stock returns:
DAL = pd.read_pickle('C:/Users/pimpe/Documents/GitHub/case_fe/data/DAL_timeseries.pkl')
dfX = pd.read_csv('stock_returns.csv')
x = dfX['x'].values


# Initial Parameters
omega_ini = 0.1  # initial value for omega
alpha_ini = 0.2  # initial value for alpha
beta_ini = 0.8   # initial value for beta
theta_ini = np.array([omega_ini,
                      alpha_ini,
                      beta_ini])

# GARCH:
garch = GARCH(theta= theta_ini, x= x, dist= "normal")
garch.calculate_GARCH()

print('AIC:', garch.AIC)
DAL.head()
