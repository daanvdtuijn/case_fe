################################  GARCH CLASS MODEL
# pedro.i
# 12/01/21
# https://findresearcher.sdu.dk:8443/ws/files/71042140/MIDASpaper.pdf
#

import numpy as np
import pandas as pd
import scipy
import math
from scipy.optimize import basinhopping, minimize

class MGARCH():
    '''
    Class that contains the M-GARCH model
    '''

    def __init__(self, theta, x, m, K):
        '''
        :param theta: initial parameters
        :param x:     stock returns
        :param m:     macro variable
        :param K:     MIDAS lags (macro var. lags)
        '''

        self.theta    = theta
        self.K        = K
        self.x        = x
        self.m        = m
        self.T        = len(m) # temporal
        # distribution
        # self.distribution = dist

    def midas_weight(self, k):
        """
        weight(w1,w2,k) = [(k/K)**(w1-1) * (1-k/K)**(w2-1)]  /  SUM(j=1 to K): (j/K)**(w1-1) * (1-j/K)**(w2-1)
        ------------------------------------------------------------------------------------------------------
        returns the midas weight function value depending on K (number of lags
        of the macro variable) and on k (SUM(k=1, K))
        - The weight function depends on current value of k, w1, w2 and K.
        """
        w1 = self.theta[5]
        w2 = self.theta[6]
        k  = k - 0.01

        sum_phi = 0
        for i in range(1, int(self.K + 1)):
            i = i - 0.001  # in order to avoid zero-division
            sum_phi = ((i / self.K) ** (w1 - 1)) * ((1 - (i / self.K)) ** (w2 - 1)) + sum_phi

        phi_k = (k/self.K)**(w1-1)  *  (1-(k/self.K))**(w2-1)  /  sum_phi
        return phi_k

    def midas_function(self):
        """
        log(tau[t]) = omega + delta * SUM = omega + delta *sum(k=1 to K): weight(k)*M_[t-k]
        ------------------------------------------------------------------------------------------
        this is the midas function, which depends on: the weight value, parameters omega and delta,
        and on sum(k=1 to K): weight(k)*M_[t-k], where M is the macro-economic variable
        """
        # Parameters
        omega = self.theta[0]
        delta = self.theta[5]
        tau = np.ones(self.T)
        for t in range(0, self.T - 1):
            sum_tau = 0
            for k in range(1, int(self.K + 1)):  # for each t, we execute a loop to summ the K values of weight(k)*M[t-k]
                if t-k < 0:  # if t-k is negative, value of the sum = 0
                    sum_tau = 0
                else:
                    sum_tau = sum_tau + self.midas_weight(k=k) * self.m[t-k]
            exp = omega + delta*sum_tau
            tau[t+1] = math.exp(exp)
        return tau

    def short_term_volatility(self):
        """
        g[t] = (1-alpha-beta) + [alpha*(x[t] - mu)**2]/tau[t] + beta* g[t-1]
        ------------------------------------------------------------------------------------------
        this is the short term volatility function
        """
        alpha = self.theta[1]
        beta  = self.theta[2]
        mu    = self.theta[3]
        g = np.ones(self.T)
        tau = self.midas_function()
        for t in range(0, self.T-1):
            g[t+1] = (1 - alpha - beta) + ((alpha*(self.x[t] - mu)**2) / tau[t]) + beta*g[t]
        return g


    def ll_M_GARCH(self, theta, x):
        """
        LL function for MIDAS-GARCH
        """

        # tau and g, weight and short term volatility terms, respectively
        tau = self.midas_function()
        g = self.short_term_volatility()
        # Log likelihood value
        l = -(1 / 2) * np.log(2 * np.pi) - (1 / 2) * np.log(tau*g) - (1 / 2) * (x - theta[3]) / (tau*g)
        return -np.mean(l)

    def calculate_M_GARCH(self):
        """
         Optimizing LL
        """
        # Options for optimization
        options = {'eps': 1e-09,
                   'disp': False,
                   'maxiter': 200}

        # Optimizing GARCH function
        # bassinhoping!!!!!!!! set bounds

        results = scipy.optimize.minimize(self.ll_M_GARCH, self.theta, args=self.x,
                                          options=options,
                                          method='SLSQP')

        # storing results
        self.param = results.x
        self.logl = results.fun
        return results


#### MIDAS-GARCH model

# Macro-economic variable: NFCI (https://fred.stlouisfed.org/series/NFCI)
# WEEKLY!
# nfci= pd.read_csv('C:/Users/pimpe/Documents/GitHub/case_fe/data/NFCI.csv')
nfci= pd.read_csv('../../data/NFCI.csv')
nfci = nfci.iloc[17:]
nfci = nfci['NFCI'].values

# Stock returns: set to weekly data
#DAL = pd.read_pickle('C:/Users/pimpe/Documents/GitHub/case_fe/data/DAL_timeseries.pkl')
DAL = pd.read_pickle('../../data/DAL_timeseries.pkl')
DAL_weekly = DAL.resample('W').sum()
DAL_weekly = DAL_weekly.iloc[:-1]

# Initial Parameters
omega_ini  = 0.1
alpha_ini  = 0.2
beta_ini   = 0.8
mu_ini     = 0.01
delta_ini  = 0.1
w1_ini     = 0.6
w2_ini     = 0.4

theta_ini = np.array([omega_ini, alpha_ini,beta_ini,
                      mu_ini, delta_ini, w1_ini, w2_ini])

# MGARCH:
mgarch = MGARCH(theta = theta_ini, x= DAL_weekly, m = nfci, K = 4)
mgarch.midas_weight(k=1) # works
mgarch.midas_weight(k=2) # works
mgarch.midas_weight(k=3) # works
mgarch.midas_weight(k=4) # works
mgarch.midas_function() # works
# mgarch.short_term_volatility() # works
# mgarch.ll_M_GARCH() # works
mgarch.calculate_M_GARCH()
#
print(mgarch.T)
print(mgarch.param)