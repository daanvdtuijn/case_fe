################################  E-GARCH CLASS MODEL
# pedro.i
# 12/01/21

import numpy as np
import pandas as pd
import scipy
import math
from scipy.optimize import minimize

class EGARCH():
    '''
    Class that contains the E.GARCH model
    '''

    def __init__(self, theta, x, dist):
        '''
        :param theta: initial parameters
        :param x:     stock returns
        :param dist:  error distrib
        '''

        self.theta = theta
        self.x = x
        # distribution
        self.distribution = dist

    def ll_EGARCH(self, theta, x):
        """
        LL and sigma function

        """
        # Log-likelihood GARCH function
        T = len(x)
        omega = theta[0]
        alpha = theta[1]
        beta = theta[2]


        # Filter Volatility
        sig = np.zeros(T)
        sig[0] = np.var(self.x)  # initialize volatility at unconditional variance

        for t in range(0, T - 1):
            exp = omega + (abs(x[t]) + alpha*x[t])/math.sqrt(sig[t]) + beta*np.log(sig[t])
            sig[t+1] = math.exp(exp)
        ## Calculate Log Likelihood Values for normal/t-stud distribution
        if self.distribution == "normal":
            l = -(1 / 2) * np.log(2 * np.pi) - (1 / 2) * np.log(sig) - (1 / 2) * (self.x ** 2) / sig
        elif self.distribution == "student":
            pass
        ## Storing the volatility
        self.sigma = sig
        return float(-np.mean(l))


    def calculate_EGARCH(self):
        """
        Optimizing LL function, storing results

        """
        # EGARCH stationarity condition (sum of alpha less_than 1)

        def stat_cond(theta):
            alpha = theta[1]
            stat_cond = 1 - alpha
            return stat_cond

        # Options for optimization
        options ={'eps':1e-09,
                  'disp': False,
                  'maxiter':200}

        # Optimizing GARCH function
        results = scipy.optimize.minimize(self.ll_EGARCH, self.theta, args= self.x,
                                           options=options,
                                          method='SLSQP', bounds=((0.00001, 100),(0, 10),(0, 1)),
                                          constraints = {"fun": stat_cond, 'type': 'ineq'})

        # storing results
        self.param = results.x
        self.logl = results.fun
        self.AIC = -2*self.logl+2*len(self.param)
        self.resid = (self.x / self.sigma)
        return results


################################

### EXAMPLE WITH GARCH MODEL
## Inputs:
# Stock returns
# DAL = pd.read_pickle('../data/DAL_timeseries.pkl')
dfX = pd.read_csv('stock_returns.csv')
x = dfX['x'].values


# Initial Parameters
omega_ini = 0.1  # initial value for omega
alpha_ini = 0.2  # initial value for alpha
beta_ini = 0.8   # initial value for beta
theta_ini = np.array([omega_ini,
                      alpha_ini,
                      beta_ini])

# EGARCH:
egarch = EGARCH(theta= theta_ini, x= x, dist="normal")
egarch.calculate_EGARCH()

print('AIC:', egarch.AIC)
