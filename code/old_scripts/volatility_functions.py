import pandas as pd
import numpy as np
import math
import datetime
import matplotlib.pyplot as plt

DAL = pd.read_pickle('../data/DAL_timeseries.pkl')
index_volas = pd.read_pickle('../data/IXIC_volas.pkl')

####### 1) REALIZED VARIANCE
def sum_squared_log_returns(one_day_prices):

    '''sample variance from slide 21'''

    if (len(one_day_prices) > 0):
        RV = np.sum( np.diff(np.log(one_day_prices))**2 )
    else: 
        RV = None
    
    return RV

def calcRV(X: pd.Series):

    ''' calculates realized variance of given timeseries X'''

    RV = X.resample("1D").apply(sum_squared_log_returns)
    RV = RV.dropna()

    return RV
 
def calcReturns(stock):
    
    open_prices = stock.resample("1D").first().dropna()
    close_prices = stock.resample("1D").last().dropna()
    
    Returns = np.log(open_prices) - np.log(close_prices)
    
    return Returns


######## 2) Realized Kernel

def calc_rv_sparse(one_day_prices):
    "1. Input: daily prices"
    "2. Generate time series of 1 second frequency and forward fill nans"
    "3.1 Compute 1200 different time series of 20 minute returns."
    "3.2 For each of these 1200 time series the realized variance (RV) is computed"
    "3.3 rv_sparse is the average of the 1200 realized variances"
    
    "Ouput: rv_sparse which will be used in bandwith_selection()"
    
    nr_ts = 1200
    
    ts_seconds = one_day_prices.resample('1S').pad()
    runs = int(len(ts_seconds)/nr_ts)
    
    realized_variances = np.zeros((nr_ts,1))
    rv_sparse = 1/nr_ts * np.sum(realized_variances)
    
    return rv_sparse

def calc_omega_rk(one_day_prices):
    "Input: daily prices"
    "Ouput: omega which will be used in bandwith_selection()"
    
    omega = 1
    
    return omega

def bandwith_selection(one_day_prices):
    
    "input: daily prices"
    "output: optimal bandwith according to provided formula, which is used in calc_rk()"
    
    n = len(one_day_prices) - 1    
    rv_sparse = calc_rv_sparse(one_day_prices)
    omega = calc_omega_rk(one_day_prices)
    chi = np.sqrt(omega/rv_sparse)    
    
    bandwith = 3.5134 * chi**0.8 * n**0.6
    
    return math.ceil(bandwith)

def calc_parzen_kernel(number):
    
    "input: number"
    "output: non-negative estimate using kernel weight function, which is used in calc_rk()"
    
    number = abs(number) 
    
    if (0 <= number <= 0.5):
        parzen_estimate = 1 - 6*number**2 + 6*number**3
    elif (0.5 <= number <= 1):
        parzen_estimate = 2*(1-number)**3
    else:
        parzen_estimate = 0
        
    return parzen_estimate


def calc_rk(one_day_prices):
    
    "input: daily transactions/prices, that still have to be subsampled"
    "output: estimated realized kernel"
    
    #use subsample formula here
    subsample = one_day_prices
    
    optimal_bandwith = bandwith_selection(subsample)
    bandwith_range = np.array(-optimal_bandwith, optimal_bandwith)
    ret = np.log(subsample).diff().dropna()
    n = len(ret)
    
    rk = 0
    for h in bandwith_range:
        gamma = 0
        for j in range((abs(h)+1),n):
            gamma = gamma + ret[j]*ret[j-abs(h)]
        k = calc_parzen_kernel(h/(bandwith_range + 1))
        rk = rk + k*gamma
    
    return rk


    
    
