# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 10:19:53 2021

https://www.kevinsheppard.com/teaching/python/notes/notebooks/example-gjr-garch/

@author: jensb
"""

import pandas as pd
import numpy as np
from scipy.optimize import minimize
from scipy.optimize import fmin_slsqp
import matplotlib.pyplot as plt


class GJR_GARCH():
    

    '''
    Model: GJR_GARCH
    '''

    def __init__(self, x):
        
        self.mu = x.mean()
        self.x = x
        # omega, alpha, beta, gamma
        self.theta_ini = np.array([0.01 , 0.2,  0.9, 0.1])*x.var()
        self.estimated_model = None

    def filtering(self, theta):

        T = len(self.x)
        sig = np.zeros(T)
        sig[0] = np.var(self.x)
        
        omega = theta[0]
        alpha = theta[1]
        beta = theta[2]
        gamma = theta[3]
        
        # use model to filter
        for t in range(0,T-1):
            sig[t+1] = omega + alpha*self.x[t]**2 + gamma *self.x[t]**2 * (self.x[t]<0) + beta*sig[t]
        return sig

    def log_likelihood(self, theta):

        sig = self.filtering(theta)
        # score to be optimized
        l = -(1/2)*np.log(2*np.pi) - (1/2)*np.log(sig) - (1/2)*(self.x**2)/sig
        return -np.mean(l)
    

    def fit(self, bnds = ((0.0000001, 100), (0, 1), (0, 1), (0,1)), mthd = 'SLSQP', **kwargs):

        # options for optimization
        options ={'eps':1e-09,
                  'disp': False,
                  'maxiter':200}
        
        def constraint(theta):
            
            alpha = theta[1]
            gamma = theta[3]
            beta = theta[2]

            return 1-alpha-gamma/2-beta
        
        constraints={"fun": constraint, 'type': 'ineq'}
        
        # for k, v in kwargs.items():
        #     if k in options.keys():
        #         options[k] = v

        result = minimize(self.log_likelihood, self.theta_ini, 
                          options = options, bounds = bnds, method = mthd, constraints = constraints)
        
        # result_2 = fmin_slsqp(self.log_likelihood, self.theta_ini, 
        #                   bounds = bnds, f_ieqcons=self.gjr_constraint, epsilon = 1e-09, disp = False, iter = 200)
              
        # set new attributes!
        self.params = result.x
        self.llv = result.fun
        self.sig = self.filtering(theta=self.params)
        self.resid = (self.x / self.sig)
        
        return result
    
    # def gjr_constraint(self, theta, out=None):
    #     ''' Constraint that alpha+gamma/2+beta<=1'''
    
    #     alpha = theta[1]
    #     gamma = theta[3]
    #     beta = theta[2]

    #     return np.array([1-alpha-gamma/2-beta])                 
    
        
    def forecast(self):
        raise NotImplementedError
                
        
DAL = pd.read_pickle('../../data/DAL_timeseries.pkl')
dfX = pd.read_csv('stock_returns.csv')
x = dfX['x'].values

daily_prices_cc = DAL.resample('1D').agg('last').dropna()
daily_log_diff = np.log(daily_prices_cc).diff().dropna()*100

gjr_garch = GJR_GARCH(daily_log_diff)
gjr_garch.fit()
print('params:', gjr_garch.params)
print('log likelihood:', gjr_garch.llv)




from arch import arch_model
am = arch_model(daily_log_diff, p=1, o=1, q=1)
res = am.fit(update_freq=5, disp='off')
print(res.summary())