import numpy as np
from numpy.linalg import inv, norm

def newton(func, x0, param, error, maxit):
    # Input: a function (func), an initial value (x0),
    #        the parameters of the function (param),
    #        the error set for the optim. (if Xt-Xt-1 =< error -> stop),
    #        the maximun number of iterations (maxit)
    # Output: Max value of the function.
    #
    #
    h = np.diag(x0*1.e-4)
    n = len(x0)
    J = np.empty((n, n))

    for i in range(0, maxit):
        f = func(x0, param)
        for j in range(n):
            J[:, j] = (f-func(x0-h[:, j], param))/h[j, j]
        x = x0 - np.matmul(inv(J), f)
        if norm(x-x0) < error:
            break
        x0 = x
    if i >= maxit:
        print('Reached max number of iterations')
    return x